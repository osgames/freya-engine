<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="map"/>
	</xsl:template>
	
	<xsl:template match="map">
		<html>
			<body>
			<table border="0" cellpadding="0" cellspacing="0" >
				<xsl:apply-templates select="row"/>
			
			</table>
			</body>
		</html>
	
	</xsl:template>
	
	<xsl:template match="row">
		<tr>
			<xsl:for-each select="location" >
				<td>
					<xsl:choose>
						<xsl:when test="terrain='blocked'">
							<table bgcolor="black">
								<tr>
									<td>
										<img src="file://c:/sasha/freya/xml/images/black.gif" alt=""/>
									</td>
									<td><img src="file://c:/sasha/freya/xml/images/black.gif" alt=""/>
</td>
								</tr>
								<tr>
									<td><img src="file://c:/sasha/freya/xml/images/black.gif" alt=""/>
</td><td><img src="file://c:/sasha/freya/xml/images/black.gif" alt=""/>
</td>
								</tr>
							</table>					
						</xsl:when>
						<xsl:otherwise>
						<table>
						<tr>
							<xsl:apply-templates select="inventory"/>
							<xsl:apply-templates select="terrain"/>
						</tr>
						<tr>
						<xsl:choose>
							<xsl:when test="unit">
								<td>
								<img src="file://c:/sasha/freya/xml/images/creature.gif" alt=""/>

								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><img src="file://c:/sasha/freya/xml/images/white.gif" alt=""/>
</td>
							</xsl:otherwise>
						</xsl:choose>

						<td><img src="file://c:/sasha/freya/xml/images/white.gif" alt=""/>
</td>
					
					</tr>
					</table>
						</xsl:otherwise>
					</xsl:choose>
					
				</td>
			</xsl:for-each>
		</tr>
	</xsl:template>
	
	<xsl:template match="inventory">
			<xsl:if test=".=''">
				<td >
					<img src="file://c:/sasha/freya/xml/images/white.gif" alt=""/>
				</td>
			</xsl:if>
		<xsl:apply-templates select="item"/>
	</xsl:template>
	
	<xsl:template match="item">
		<xsl:apply-templates select="value"/>
	</xsl:template>
	
	<xsl:template match="value">
		<xsl:if test=".=1">
			<td>
				<img src="file://c:/sasha/freya/xml/images/crystal1.gif" alt=""/>
			</td>
		</xsl:if>
		<xsl:if test=".=2">
			<td>
<img src="file://c:/sasha/freya/xml/images/crystal2.gif" alt=""/>

			</td>
		</xsl:if>
		<xsl:if test=".=3">
			<td>
<img src="file://c:/sasha/freya/xml/images/crystal3.gif" alt=""/>

			</td>
		</xsl:if>
		<xsl:if test=".>3">
			<td>
<img src="file://c:/sasha/freya/xml/images/crystal4.gif" alt=""/>

			</td>
		</xsl:if>
		<xsl:if test=".=0">
			<td>
				<img src="file://c:/sasha/freya/xml/images/white.gif" alt=""/>

			</td>
		</xsl:if>

	</xsl:template>
	
	<xsl:template match="terrain">
			<xsl:if test=".='blocked'">
				<td bgcolor="black">
					<xsl:text>#</xsl:text>
				</td>
			</xsl:if>
			<xsl:if test=".='open'">
				<td bgcolor="white">
					<xsl:text> </xsl:text>
				</td>
			</xsl:if>

	</xsl:template>
	<xsl:template match="unit">
		<td>
			<xsl:text>*</xsl:text>
		</td>
	</xsl:template>

	
</xsl:stylesheet>
