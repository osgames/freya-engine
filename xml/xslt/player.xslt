<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">

		<span>

			<html>

				<head>

					<link rel="stylesheet" href="../css/font.css" type="text/css"/>

 				</head>

				<body background="../images/background3.gif">



				<form action="/freya-working/servlet/com.eoi.freya.servlet.CommandCapture" method="post">



				<xsl:apply-templates select="player_doc">



				</xsl:apply-templates>

				

				<table border="1" cellspacing="0" cellpadding="0"><tr><td> <input class="boxborder" type="submit" value="SUBMIT TURN"/></td></tr>
</table>
				</form>

				</body>

			</html>

		</span>

	</xsl:template>

	

				<xsl:template match="player_doc">

					<xsl:apply-templates select="player_name"/>
					<xsl:apply-templates select="timestamp"/>
	
				 <xsl:apply-templates select="unit" mode="overview"/>

				</xsl:template>

				

				<xsl:template match="player_name">

				<h2><img src="../images/bullet.jpg"/><xsl:text>Player : </xsl:text><xsl:value-of select="."/></h2>

				<input type="hidden" name="{.}_name" value="{.}"/>	

				</xsl:template>

	<xsl:template match="timestamp">
	<p><xsl:text>timestamp:</xsl:text><xsl:value-of select="."/></p>
	</xsl:template>


				<xsl:template match="unit" mode="overview">
					<table border="1" cellpadding="0" cellspacing="0" bgcolor="green">
					 <tr>
					<td>

				<h3><xsl:text>Unit : </xsl:text><xsl:value-of select="name"/></h3>

					<xsl:apply-templates select="map"/>
					<xsl:apply-templates select="spot"/>

					<br></br>

					<xsl:apply-templates select="stats"/>
					</td>
					 </tr>
					</table>
					<br></br>

				</xsl:template>



				

	<xsl:template match="stats">

		<xsl:apply-templates select="unit-result"></xsl:apply-templates>

	</xsl:template>

	

	<xsl:template match="unit-result">

					<table width="90%">

						<tr>

							<td><p class="tabhead">Race</p></td>

							<td><p class="tabhead">Name</p></td>

							<td><p class="tabhead">Attack</p></td>

							<td><p class="tabhead">Defence</p></td>

							<td><p class="tabhead">Health</p></td>

							<td><p class="tabhead">Retreat</p></td>

							<td><p class="tabhead">Crystals</p></td>

							<td><p class="tabhead">Move</p></td>

							<td><p class="tabhead">Cost</p></td>

							<td><p class="tabhead">Location</p></td>

							<td><p class="tabhead">Type</p></td>



							</tr>



							<tr>

								<td>

									<xsl:apply-templates select="race"/>

								</td>

								<td>

									<xsl:apply-templates select="name"/>

								</td>

								<td>

									<xsl:apply-templates select="attack"/>

								</td>

								<td>

									<xsl:apply-templates select="defence"/>

								</td>

								<td>

									<xsl:apply-templates select="health"/>

								</td>

								<td>

									<xsl:apply-templates select="retreat"/>

								</td>



								<td>

									<xsl:apply-templates select="inventory"/>

								</td>

								<td>

									<xsl:apply-templates select="movement-points"/>

								</td>

								<td>

									<xsl:apply-templates select="cost"/>

								</td>

										<td>

											<xsl:for-each select="position">

												<xsl:for-each select="@x">

													<xsl:value-of select="."/>

												</xsl:for-each>,<xsl:for-each select="@y">

													<xsl:value-of select="."/>

												</xsl:for-each>

											</xsl:for-each>

										</td>

										<td>

									<xsl:apply-templates select="type"/>

										</td>





									</tr>



								</table>

					<table>			

					<input type="hidden" name="{name}_move" value="{name}"/>

					<input type="hidden" name="{name}_move_name" value="{name}"/>

					<tr><td><p class="tabhead"><xsl:text>Move </xsl:text></p></td><td><input type="text" name="{name}_move_directions" class="blackbox"/></td><td></td><td></td></tr>

					

					<input type="hidden" name="{name}_take" value="{name}"/>

					<input type="hidden" name="{name}_take_name" value="{name}"/>

					<input type="hidden" name="{name}_take_type" value="crystals"/>

					<tr><td><p class="tabhead"><xsl:text>Take </xsl:text></p></td><td><input type="text" name="{name}_take_value" class="blackbox"/></td><td><p class="tabhead"><xsl:text> Crystals </xsl:text></p></td><td></td></tr>

					

					<input type="hidden" name="{name}_drop" value="{name}"/>

					<input type="hidden" name="{name}_drop_name" value="{name}"/>

					<input type="hidden" name="{name}_drop_type" value="crystals"/>

					<tr><td><p class="tabhead"><xsl:text>Drop </xsl:text></p></td><td><input type="text" name="{name}_drop_value" class="blackbox"/></td><td><p class="tabhead"> <xsl:text>Crystals </xsl:text></p></td><td></td></tr>

					

					<input type="hidden" name="{name}_give" value="{name}"/>

					<input type="hidden" name="{name}_give_name" value="{name}"/>

					<input type="hidden" name="{name}_give_type" value="crystals"/>	

					<tr><td><p class="tabhead"><xsl:text>Give </xsl:text></p></td><td><input type="text" name="{name}_give_target" class="blackbox"/></td><td><input type="text" name="{name}_give_value" class="blackbox"/></td><td><p class="tabhead"><xsl:text> Crystals </xsl:text></p></td></tr>

					</table>



	</xsl:template>



				<xsl:template match="race">

					<xsl:value-of select="."/>

				</xsl:template>

				

				<xsl:template match="name">

					<xsl:value-of select="."/>

				</xsl:template>

				

				<xsl:template match="attack">

					<xsl:value-of select="value"/>

					<xsl:text>\</xsl:text>

					<xsl:value-of select="max"/>

				</xsl:template>

				

				<xsl:template match="health">

					<xsl:value-of select="value"/>

					<xsl:text>\</xsl:text>

					<xsl:value-of select="max"/>

				</xsl:template>

				

				<xsl:template match="defence">

					<xsl:value-of select="value"/>

					<xsl:text>\</xsl:text>

					<xsl:value-of select="max"/>

				</xsl:template>

				

				<xsl:template match="movement-points">

					<xsl:value-of select="value"/>

					<xsl:text>\</xsl:text>

					<xsl:value-of select="max"/>

				</xsl:template>

				

				<xsl:template match="retreat">

					<xsl:value-of select="value"/>

				</xsl:template>

				<xsl:template match="inventory">

					<xsl:value-of select="item/value"/>

				</xsl:template>

				

				<xsl:template match="map">



			<table border="1" cellpadding="0" cellspacing="0" >

				<xsl:apply-templates select="row"/>

			

			</table>

	

	</xsl:template>

	

	<xsl:template match="row">

		<tr>

			<xsl:for-each select="location" >

				<td>

					<xsl:choose>

						<xsl:when test="terrain='blocked'">

							<table border="0" cellpadding="0" cellspacing="0" bgcolor="black">

								<tr>

									<td background="../images/rock.gif"> <img src="../images/trans.gif" alt="Rock"/></td><td background="../images/rock.gif"> <img src="../images/trans.gif" alt="Rock"/></td>

								</tr>

								<tr>

									<td background="../images/rock.gif"> <img src="../images/trans.gif" alt="Rock"/></td><td background="../images/rock.gif"> <img src="../images/trans.gif" alt="Rock"/></td>

								</tr>

							</table>					

						</xsl:when>

						<xsl:otherwise>

						<table border="0" cellpadding="0" cellspacing="0">

						<tr>

							<xsl:apply-templates select="inventory" mode="map"/>

							<xsl:apply-templates select="terrain"/>

						</tr>

						<tr>

						<xsl:choose>

							<xsl:when test="unit">

								<td background="../images/white.gif"><img src="../images/mother.gif" alt="{.}"/></td>

							</xsl:when>

							<xsl:otherwise>

								<td background="../images/white.gif"><img src="../images/trans.gif" alt="Open"/></td>

							</xsl:otherwise>

						</xsl:choose>

						<td background="../images/white.gif"><img src="../images/trans.gif" alt="Open"/></td>

						</tr>

						</table>

						</xsl:otherwise>

					</xsl:choose>

				</td>

			</xsl:for-each>

		</tr>

	</xsl:template>

	

	<xsl:template match="inventory" mode="map">

			<xsl:if test=".=''">

				<td background="../images/white.gif"><img src="../images/trans.gif" alt="Open"/></td>

			</xsl:if>

		<xsl:apply-templates select="item" mode="map"/>

	</xsl:template>

	

	<xsl:template match="item" mode="map">

		<xsl:apply-templates select="value" mode="map" />

	</xsl:template>

	

	<xsl:template match="value" mode="map">

		<xsl:if test=".>0">

			<td background="../images/white.gif"><img src="../images/crystal1.gif" alt="Crystal"/></td>

		</xsl:if>

		<xsl:if test=".=0">

			<td background="../images/white.gif"><img src="../images/trans.gif" alt="Open"/></td>

		</xsl:if>



	</xsl:template>

		<xsl:template match="terrain">

			<xsl:if test=".='blocked'">

				<td background="../images/rock.gif"> <img src="../images/trans.gif" alt="Rock"/></td>

			</xsl:if>

			<xsl:if test=".='open'">

				<td background="../images/white.gif"><img src="../images/trans.gif" alt="Open"/></td>

			</xsl:if>

	</xsl:template>



<xsl:template match="spot">
	<xsl:apply-templates select="unit" mode="spot"/>
</xsl:template>


<xsl:template match="unit" mode="spot">
 <p class="alert"><xsl:value-of select="."/><xsl:text> is also at this location.</xsl:text></p>
</xsl:template>







</xsl:stylesheet>
