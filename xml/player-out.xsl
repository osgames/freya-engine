<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="/">
	<xsl:apply-templates select="player"/>
</xsl:template>

<xsl:template match="player">
<html>
<body>
	<p>
		<h2>
		<xsl:value-of select="name"/>
		</h2>
		<p>
		<xsl:apply-templates select="units-list"/>
		</p>
	</p>
</body>
</html>
</xsl:template>

<xsl:template match="units-list">
Units:

	<xsl:apply-templates select="unit"/>

<br></br>
</xsl:template>

<xsl:template match="unit">
<table>
<tr>
<td>Name</td>
<td>
<b>
	<xsl:value-of select="name"/>
</b>

</td>
</tr>
<tr>
<td>Attack</td>
<td>
	<xsl:value-of select="attack/value"/>
</td>
</tr>
<tr>
<td>Defence</td>
<td>
	<xsl:value-of select="defence/value"/>
</td>
</tr>
<tr>
<td>Health</td>
<td>
	<xsl:value-of select="health/value"/>
</td>
</tr>


</table>
</xsl:template>


</xsl:stylesheet>
