<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head/>
			<body>
				<xsl:for-each select="map">
					<xsl:for-each select="row">
						<xsl:if test="position()=1">
							<xsl:text disable-output-escaping="yes">&lt;table border="1"&gt;</xsl:text>
						</xsl:if>
						<xsl:if test="position()=1">
							<thead>
								<tr>
									<td>location</td>
								</tr>
							</thead>
						</xsl:if>
						<xsl:if test="position()=1">
							<xsl:text disable-output-escaping="yes">&lt;tbody&gt;</xsl:text>
						</xsl:if>
						<tr>
							<td>
								<xsl:for-each select="location">
									<xsl:for-each select="inventory">
										<xsl:for-each select="item">
											<xsl:for-each select="value">
												<xsl:apply-templates/>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</td>
						</tr>
						<xsl:if test="position()=last()">
							<xsl:text disable-output-escaping="yes">&lt;/tbody&gt;</xsl:text>
						</xsl:if>
						<xsl:if test="position()=last()">
							<xsl:text disable-output-escaping="yes">&lt;/table&gt;</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
