<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="map"/>
	</xsl:template>
	
	<xsl:template match="map">
		<html>
			<body>
			<table border="0">
				<xsl:apply-templates select="row"/>
			
			</table>
			</body>
		</html>
	
	</xsl:template>
	
	<xsl:template match="row">
		<tr>
			<xsl:for-each select="location" >
					<xsl:apply-templates select="terrain"/>
			</xsl:for-each>
		</tr>
	</xsl:template>
	
	<xsl:template match="terrain">
			<xsl:if test=".='blocked'">
				<td bgcolor="black">
					<xsl:text>#</xsl:text>
				</td>
			</xsl:if>
			<xsl:if test=".='open'">
				<td bgcolor="white">
					<xsl:text> </xsl:text>
				</td>
			</xsl:if>

	</xsl:template>
	
	
	
</xsl:stylesheet>
