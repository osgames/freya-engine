package com.eoi.freya.servlet;

import org.jdom.*;
import org.jdom.output.*;

import java.util.*;
import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

/**
 * Simple servlet that filters Move commands from a web page.
 *
 * @author     Sasha Bilton
 * @created    14 March 2002
 * @version    1.0
 */

public class CommandCapture extends HttpServlet {

  private HashMap names = new HashMap();
  private String playerName = null;


	/**
	 *Description of the Method
	 *
	 * @param  req                   Description of Parameter
	 * @param  res                   Description of Parameter
	 * @exception  ServletException  Description of Exception
	 * @exception  IOException       Description of Exception
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {


		names = new HashMap();

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		out.println("<html><body>");

		Enumeration enum = req.getParameterNames();




                //go through each parameter and process the fiedl, turning it into A JDOM element
		while (enum.hasMoreElements())
                {
                  String name = (String)enum.nextElement();
                  String value = req.getParameter(name);

                  process(name,value, out);


		}

                //produce and XML files from the fields.
                xmlOut(out);

	}


	/**
	 *Redirect to POST
	 *
	 * @param  req                   Description of Parameter
	 * @param  res                   Description of Parameter
	 * @exception  ServletException  Description of Exception
	 * @exception  IOException       Description of Exception
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}


        private void process(String name,String value, PrintWriter out){

          out.println("<p> processing "+name+"="+value+"<br>");
          StringTokenizer st = new StringTokenizer(name,"_");

          //get the unit or player name...
          String unit = st.nextToken();

           HashMap elements;
          if (names.containsKey(unit))
          {
            elements = (HashMap)names.get(unit);
            out.println("Unit found : "+unit+"<br>");
          }
          else
          {
            elements = new HashMap();
            names.put(unit, elements);
            out.println("Unit created : "+unit+"<br>");
          }

          if (st.hasMoreTokens())
          {
            String firstCommand = st.nextToken();
            {
              Element firstElement;
              if (elements.containsKey(firstCommand))
              {
                firstElement = (Element)elements.get(firstCommand);
                out.println("first element found : "+firstCommand+"<br>");
              }
              else
              {
                firstElement = new Element(firstCommand);
                out.println("firstElement create : "+firstCommand);
                elements.put(firstCommand, firstElement);

              }

              if (!st.hasMoreTokens())
              {
              	firstElement.addContent(value);
                out.println("first element set to : "+value+"<br>");
              }
              else
              {
                  String secondCommand = st.nextToken();
                  Element secondElement = new Element(secondCommand);
                  secondElement.addContent(value);
                  firstElement.addContent(secondElement);
                  out.println("Second element created : "+secondCommand+" and set to : "+value+"<br>");

              }


            }
          }

       }

      private void xmlOut(PrintWriter out)
      {
	  out.println("<p>Starting to create XML Document...");
          Element pc = new Element("player-commands");
	  Element commands = new Element("commands");
	  Document doc = new Document(pc);
	  pc.addContent(commands);

          Iterator inames = names.values().iterator();
          while (inames.hasNext())
          {
            Iterator iunits = ((HashMap)inames.next()).values().iterator();
            while (iunits.hasNext())
            {
              Element element = (Element)(iunits.next());
         //     if (element.isRootElement())
              {
                commands.addContent(element);
                out.println("adding Element "+element.getName());
              }
            }
          }

	//Move the <name> tag to the root Element as expect by PlayerCommands.java
	Element nameElement = commands.getChild("name");

	if (nameElement!=null)
        {
	   nameElement.detach();
           pc.addContent(nameElement);
	}

          try {

            String name = "/home/admin/freya-working/xml/in/"+nameElement.getTextTrim()+"-commands.xml";

		out.println("<p> writing to "+name);

            FileOutputStream fos = new FileOutputStream(name);

            XMLOutputter xo = new XMLOutputter("   ", true);
            xo.setTextNormalize(true);
            xo.output(doc, out);
            xo.output(doc, fos);
            fos.flush();
            fos.close();

            out.println("</body></html>");

            out.close();
          }
          catch(Exception e)
          {
            out.println("<p>"+e);
          }
        }




	/**
	 *Gets the unitName attribute of the CommandCapture object
	 *
	 * @param  parameterName  Description of Parameter
	 * @return                The unitName value
	 */
	private String getUnitName(String parameterName) {
		return parameterName.substring(parameterName.lastIndexOf("_") + 1);
	}

}

/*
 *  Freya Engine, The Turn Based Game Engine
 *  Copyright(c) 2002 Alexander Bilton
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

