package com.eoi.freya.util;

/**
 * SafeCast is a utility class for converting (casting) one type to another, without throwing exceptions die to bad input.
 *
 * @author Sasha Bilton
 * @version 0.1
 */

public class SafeCast {

  /**
   * Convert to String to an int
   */
  public static int toInt(String value)
  {


    try
    {
        if (value.trim().equals(""))
        {
          return 0;
        }
       return Integer.parseInt(value);
    }
    catch(Exception nfe)
    {
      return 0;
    }
  }

}