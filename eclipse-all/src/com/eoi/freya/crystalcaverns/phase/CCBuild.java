package com.eoi.freya.crystalcaverns.phase;

import org.jdom.Element;

import com.eoi.freya.Log;

import com.eoi.freya.game.*;
import com.eoi.freya.basic.*;
import com.eoi.freya.crystalcaverns.unit.BuilderUnit;

/**
 * CCBuild handles Build commands
 * @author Sasha Bilton
 * @version 1.0
 */

public class CCBuild extends BasicUnitPhase {

  public CCBuild()
  {
        command = BDef.BUILD;
  }
  public Element executeForUnit(BasicUnit unit, Element commandElement)
  {
    Element result;

    if (unit instanceof BuilderUnit && unit.isSetting(BDef.CAN_BUILD))
    {
      BuilderUnit builder = (BuilderUnit)unit;
      result = builder.build(unit, commandElement.getChildTextTrim(BDef.TARGET), commandElement.getChildTextTrim(BDef.TYPE));
    }
    else
    {
      result = new Element(BDef.BUILD);
      result.setAttribute("failed","true");
      result.setText("Unit cannot build");
    }
    return result;

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/