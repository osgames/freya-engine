package com.eoi.freya.crystalcaverns.unit;

import com.eoi.freya.Log;
import com.eoi.freya.game.*;
import com.eoi.freya.basic.*;

import org.jdom.*;

import java.util.*;

/**
 * BurrowerUnits can big through 'BLOCKED' terrain, making the terrain 'OPEN'.
 * @author Sasha Bilton
 * @version 1.0
 */


public class BurrowerUnit extends BasicUnit
{



  public BurrowerUnit(Element unitInfo)
  {
    super(unitInfo);
  }


  public Element move(String directions, String modifier)
  {

    BasicMap map = BasicMap.getInstance();
    StringTokenizer commands = new StringTokenizer(directions,BDef.DELIMITER);
    BasicPosition position = getPosition();
    Element result = new Element(BDef.MOVE);
    boolean notFailed = true;

    map.getLocation(getPosition().getX(),getPosition().getY()).removeUnit(this);


    int movePoints = getMovementPoints().getValue();


    while(commands.hasMoreTokens() && notFailed)
    {
      String dir = commands.nextToken().toLowerCase().trim();

      int newX = position.getX();
      int newY = position.getY();

      if (dir.equals("n"))
      {
        newX = position.getX()-1;
      }
      if (dir.equals("s"))
      {
        newX = position.getX()+1;
      }
      if (dir.equals("e"))
      {
        newY = position.getY()+1;
      }
      if (dir.equals("w"))
      {
        newY = position.getY()-1;
      }
      if (dir.equals("ne"))
      {
        newX = position.getX()-1;
        newY = position.getY()+1;
      }
      if (dir.equals("nw"))
      {
        newX = position.getX()-1;
        newY = position.getY()-1;
      }
      if (dir.equals("se"))
      {
        newX = position.getX()+1;
        newY = position.getY()+1;
      }
      if (dir.equals("sw"))
      {
        newX = position.getX()+1;
        newY = position.getY()-1;
      }


      if ((newX > -1 && newX < map.getMaxX()) && (newY >-1 && newY < map.getMaxY()))
      {
        Location loc = map.getLocation(newX,newY);
        loc.setTerrain(BDef.OPEN);
        result.addContent(dir+",");
        position.setX(newX);
        position.setY(newY);
        Log.logger.info("new X "+position.getX()+" Y "+position.getY());
        hasMoved = true;
      }
      else
      {
        notFailed = false;
        result.setAttribute("failed","true");
        result.addContent("off map");
        Log.logger.info("move failed");
      }
      movePoints--;
      if( movePoints <1)
      {
        notFailed = false;
      }
    }

    map.getLocation(getPosition().getX(),getPosition().getY()).addUnit(this);

    return result;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/