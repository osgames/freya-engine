package com.eoi.freya.crystalcaverns.unit;

import com.eoi.freya.Log;
import com.eoi.freya.game.*;
import com.eoi.freya.basic.*;
import com.eoi.freya.crystalcaverns.CCDef;

import org.jdom.*;



/**
 * BuilderUnits can build new units using crystals.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BuilderUnit extends StandardUnit {

  public BuilderUnit(Element unitInfo)
  {
    super(unitInfo);
  }

  public Element build(BasicUnit parent, String name, String type)
  {
   Element result = new Element(BDef.BUILD);

    Inventory inventory = parent.getInventory();
    int crystals = inventory.getItemValue(CCDef.CRYSTALS);
    if (parent.isSetting(BDef.CAN_BUILD))
    {
      if (crystals >0 && !(parent.hasMoved || parent.hasAttacked || !parent.isDead()))
      {
        String race = parent.getRace();

        BasicUnit newUnit = (BasicUnit)UnitFactory.createNewUnit(new String(race), new String(type));

        int cost = newUnit.getCost().getValue();

        Log.logger.info("Cost = "+cost+ " crystals = "+crystals);
        crystals -= cost;

        if (crystals > -1)
        {

          newUnit.setOwner(parent.getOwner());
          newUnit.setName(name);
          BasicPosition bp =  newUnit.getPosition();
          bp.setX(parent.getPosition().getX());
          bp.setY(parent.getPosition().getY());

          BasicMap map = BasicMap.getInstance();
          Location loc = map.getLocation(bp);
          loc.addUnit(newUnit);

          BasicPlayerList players = BasicPlayerList.getInstance();
          BasicPlayer player = (BasicPlayer)players.getPlayer(newUnit.getOwner());
          player.addUnit(newUnit);

          inventory.setItemValue(CCDef.CRYSTALS, crystals);

          result.setText("built "+newUnit.getName()+" the "+newUnit.getSetting(BDef.TYPE)+"");

        }
        else
        {
          result.setAttribute("failed","true");
          result.setText("didn't have enough Crystals");
        }
      }
      else
      {
        result.setAttribute("failed","true");
        result.setText("was unable to build.");
      }
    }
    else
    {
      result.setText("can't build.");
    }
    return result;

  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/