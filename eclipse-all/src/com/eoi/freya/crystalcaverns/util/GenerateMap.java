package com.eoi.freya.crystalcaverns.util;

import com.eoi.freya.basic.*;
import com.eoi.freya.game.*;
import com.eoi.freya.crystalcaverns.CCDef;
import com.eoi.freya.util.Config;




import java.util.Random;

/**
 * Standard alone app for generating random tunnels and adding crystals
 * @author Sasha Bilton
 * @version 1.0
 */

public class GenerateMap {

  private BasicMap map;
  private Random rnd;

  public GenerateMap()
  {
    map = BasicMap.getInstance();
    rnd = new Random();
  }

  public void blankMap()
  {
    BasicLocation[][] locations = new BasicLocation[map.getMaxX()][map.getMaxY()];
    for (int x=0; x<map.getMaxX();x++)
    {
      for (int y=0; y<map.getMaxX();y++)
      {
        BasicLocation location = new BasicLocation();
        location.setXY(x,y);
        location.setTerrain(BDef.BLOCKED);
        locations[x][y]=location;
      }
    }
    map.setMap(locations);
  }

  public void generateTunnels()
  {

    Random rnd = new Random();
    int tunnels = (map.getMaxX() + map.getMaxY()) * 2;

    int currentX = rnd.nextInt(map.getMaxX()-5)+2;
    int currentY = rnd.nextInt(map.getMaxY()-5)+2;

    int maxLength = 0;
    int modX=0;
    int modY=0;
    int direction=0;

    for (int tunnel=0; tunnel < tunnels; tunnel++)
    {
      modX = 0;
      modY = 0;
      direction = rnd.nextInt(4)+1;
      if (direction == 1 && currentX < 2)
      {
        direction = 3;
      }

      if (direction == 3 && currentX > map.getMaxX()-2)
      {
        direction = 1;
      }

      if (direction == 4 && currentY < 2)
      {
        direction = 2;
      }

      if (direction == 2 && currentY > map.getMaxY()-2)
      {
        direction = 4;
      }

      if (direction == 1)
      {
        maxLength = currentX  -1;
        modX = -1;
      }

      if (direction == 2)
      {
        maxLength = map.getMaxY() - currentY -2;
        modY = 1;
      }

      if (direction == 3)
      {
        maxLength = map.getMaxX()  - currentX -2;
        modX = 1;
      }

      if (direction == 4)
      {
        maxLength = currentY -1;
        modY = -1;
      }

      if (maxLength > 0)
      {
       maxLength = (rnd.nextInt(maxLength)+1) /( rnd.nextInt(3)+1);

        for(int length=0; length < maxLength; length++)
        {
          BasicLocation nextLocation = (BasicLocation)map.getLocation(currentX+(modX*2), currentY+(modY*2));
          if (nextLocation.getTerrain().equals(BDef.OPEN))
          {
            if (rnd.nextInt(2
            ) == 0) // stop!
            {
              length = maxLength;
            }
            else
            {
              currentX = currentX + modX;
              currentY = currentY + modY;

              ((BasicLocation)map.getLocation(currentX, currentY)).setTerrain(BDef.OPEN);

            }
          }
          else
          {
            currentX = currentX + modX;
            currentY = currentY + modY;

              ((BasicLocation)map.getLocation(currentX, currentY)).setTerrain(BDef.OPEN);

          }
        }
      }
      if (rnd.nextInt(4) == 0)
      {
        currentX = rnd.nextInt(map.getMaxX()-5)+2;
        currentY = rnd.nextInt(map.getMaxY()-5)+2;
      }
    }
  }

  public void printMap()
  {
    for (int x=0; x<map.getMaxX();x++)
    {
      for (int y=0; y<map.getMaxY();y++)
      {
        BasicLocation loc = (BasicLocation)map.getLocation(x,y);
        if (loc.getTerrain().equals(BDef.BLOCKED))
        {
          System.out.print("#");
        }
        else
        {
          System.out.print(" ");
        }
      }
      System.out.println("");
    }
  }

  public void addCrystals()
  {
    int crystals = (map.getMaxX()+map.getMaxY()) *2;

    for (int crystal = 0; crystal < crystals;)
    {
      int x = rnd.nextInt(map.getMaxX());
      int y = rnd.nextInt(map.getMaxY());

      int number = rnd.nextInt(5)+1;
      Inventory inventory = map.getLocation(x,y).getInventory();
      inventory.addToItemValue(CCDef.CRYSTALS, number);

      crystal += number;
    }
  }


  public static void main(String[] args) {
    Config.loadFromFile("xml/config.xml");
    GenerateMap generateMap = new GenerateMap();
    generateMap.blankMap();
    generateMap.generateTunnels();
    generateMap.addCrystals();
    generateMap.printMap();
    generateMap.map.write("xml/terrainmap.xml");
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
