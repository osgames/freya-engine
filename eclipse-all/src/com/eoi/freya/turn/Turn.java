package com.eoi.freya.turn;

import com.eoi.freya.*;
import com.eoi.freya.util.*;

import com.eoi.freya.player.*;

import java.util.*;
import org.jdom.*;
import org.jdom.input.*;


/**
 * Turn contains an ArrayList of Phases that it will execute for each PlayerCommand object in player command list.
 * @author Sasha Bilton
 * @version 1.0
 */

public class Turn {

  private ArrayList phaseList = new ArrayList();

  private static Turn turn = null;

  private int currentPhase = 0;

  /** Create a Turn instance that is dynamically loaded with Phase objects.
   *  The list is a an XML document that contains the full class name of each Phase class
   *  and any attributes that the phase needs.
   *  @param filename A String contain the full path and name of the phase list XML document*/
  protected Turn(String filename)
  {

    try
    {
      // set a list counter
      int currentCount = 0;

      SAXBuilder builder = new SAXBuilder();

      //create a JDOM document from the given filename
      Document d = builder.build(filename);

      // Get the root element
      Element e = d.getRootElement();

      // Create a list iterator from all the TURN elements in the Doc
      ListIterator iterate = e.getChildren(Def.TURN).listIterator();

      //Go through the list of phases
      while (iterate.hasNext())
      {
        //get the base Element
        Element phaseElement = (Element)iterate.next();

        //get the String name of the phase class
        String className = phaseElement.getChildTextTrim(Def.NAME);
        Element attributes = phaseElement.getChild(Def.ATTRIBUTES);

        // add the phase to the array of Phases
        addPhase(className,attributes,currentCount);
        currentCount++;
      }


    }
    catch(JDOMException ex)
    {
      //System.out.println(ex);
      Log.logger.severe("Config - "+ex);
    }
  }

  /** Singleton access, specifying the file name of the xml file that holds the Phase list*/
  public static Turn loadTurnList(String filename)
  {
    if (turn == null)
    {
      turn = new Turn(filename);
    }
    return turn;
  }

  /** Singleton access*/
  public static Turn getInstance() throws NullPointerException
  {
    if (turn == null)
    {
      throw new NullPointerException("Turn.getInstance called before loading");
    }
    else
    {
      return turn;
    }
  }

  /** Adds a Phase to the list, by defining the class  name, attributes and position
   *  @param className String containing the full class name of the Phase class
   *  @param attributes A Jdom Element that is used as the info block for the Phase
   *  @param position The int value of the position to place the phase in
   */
  public void addPhase(String className, Element attributes, int position)
  {
    try
    {
      // Create the class object from the class name
      Class c = Class.forName(className);

      // Create an instance of Phase super class
      Phase p = (Phase)c.newInstance();

      // if there are attributes to go with this phase set them as the Info block for the phase class
      if (attributes != null)
      {
        p.setInfo(attributes);
      }
      // otherwise create an empty info block
      else
      {
        p.setInfo(new Element(Def.ATTRIBUTES));
      }

      // Let the phase know it's name
      p.setName(className);
      // add the phase to the array in the given position
      phaseList.add(position,p);
      Log.logger.info(className+" added to TurnList");
    }
    catch(Exception e)
    {
      Log.logger.severe("Turn.addPhase - "+e);
      e.printStackTrace();
    }
  }

  /** @return Is there another phase in the list?*/
  public boolean nextPhase()
  {
    return (currentPhase <= phaseList.size()-1);
  }

  /** @return the next phase and increment the list position.*/
  public Phase getNextPhase()
  {
    return (Phase)phaseList.get(currentPhase++);
  }

  /** Run the turn, using the player command list.
   *  For each phase in the list, this method assigns the current players command block to the current phase
   *  and the calls <code>execute()</code> on the phase, unless it is a non-player
   *  (repeatForAllPlayers returns false) phase.
   */
  public void run()
  {
    // Get the list of PlayerCommand objects (one for each player)
    PlayerCommandList playerList = PlayerCommandList.getInstance();

    // Loop through all the phases
    while (nextPhase())
    {
      // Get the next Phase object
      Phase phase = getNextPhase();
      Log.logger.info("Phase "+phase.getName()+" started.");

      //If this is a phase that is repeated for all players
      if(phase.repeatForAllPlayers())
      {
        Log.logger.info("Repeating for all players.");

        //Loop through the playerCommand list
        while (playerList.nextPlayer())
        {
          //Set the phases playerCommand to that of the current player
          phase.setPlayerCommands(playerList.getNextPlayer());
          // Call the all important execute()
          phase.execute();
        }
        //Reset the player List
        playerList.reset();
      }
      else
      //It's a non player phase, so just call it.
      {
        Log.logger.info("Single phase.");
        phase.execute();
      }
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/