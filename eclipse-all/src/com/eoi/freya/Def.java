package com.eoi.freya;

/**
 * Constant defines for engine.
 * @author Sasha Bilton
 * @version 1.0
 */


public class Def {
// Config stuff
  public final static String PLAYER_COMMAND_LIST = "player-command-file";
  public final static String PLAYER_LIST = "player-list-file";
  public final static String TURN_LIST = "turn-list-file";
  public final static String PLAYER_FILE = "player-file";
  public final static String NUMBER_OF_PLAYERS = "number-of-players";
  public final static String PLAYER_DOC_DIR = "player-doc-dir";
  public final static String LOG_CONFIG_FILE ="log-config-file";
  public final static String PLAYER_FILE_DIR ="player-file-dir";
  public final static String PLAYER_FILE_OUT_DIR ="player-file-out-dir";
  public final static String DATA_DIC = "data-dic";
  public final static String MAP = "map";
  public final static String MAX_X = "max-x";
  public final static String MAX_Y = "max-y";
  public final static String GAME_CONFIG_FILE = "game-config-file";
  public final static String XSL_FILE = "xsl-file";





// Turn stuff
  public final static String TURN = "turn";
  public final static String NAME = "name";
  public final static String ATTRIBUTES = "attributes";

// Player Stuff

  public final static String PLAYER_DOC = "player_doc";
  public final static String PLAYER_NAME = "player_name";
  public final static String COMMANDS = "commands";

//Base Unit

  public final static String UNIT = "unit";
  public final static String OWNER = "owner";
  public static final String ITEM = "item";
  public static final String VALUE = "value";
  public static final String TYPE = "type";
  public static final String INVENTORY = "inventory";
  public static final String DEAD = "dead";

//UnitFactory
  public static final String UNIT_CLASS_MAP = "unit_class_map";


  private Def() {
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/