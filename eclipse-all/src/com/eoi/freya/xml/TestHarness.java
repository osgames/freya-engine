package com.eoi.freya.xml;

import com.eoi.freya.game.*;
import com.eoi.freya.util.*;
import com.eoi.freya.turn.*;
import com.eoi.freya.player.*;
import com.eoi.freya.*;
import com.eoi.freya.crystalcaverns.CCDef;
import com.eoi.freya.crystalcaverns.unit.*;
import com.eoi.freya.crystalcaverns.util.*;

import com.eoi.freya.basic.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;


/**
 * For testing stuff, ignore.
 * @author
 * @version 1.0
 */

public class TestHarness {

  SAXBuilder saxBuilder = new SAXBuilder();

  public TestHarness()
  {

  }




  public void testInitialise(String file)
  {

      Initialise i = new Initialise(file);

      //GeneratePlayer gp = new GeneratePlayer();
      //gp.initialisePlayer();

      Turn t = Turn.getInstance();
      t.run();

         //GeneratePlayer gp = new GeneratePlayer();



  }

  public void makePlayers(String file)
  {
    Initialise i = new Initialise(file);

    GeneratePlayer gp = new GeneratePlayer();
    gp.makeUnits("Yoth",CCDef.BLOB,5,5,"Snifllog","Yogtharog");
    gp.makeUnits("Myraid",CCDef.BLOB,5,5,"Seeker","Motherlode");
  }

  public static void main(String[] args)
  {
    try{
      TestHarness testHarness1 = new TestHarness();
      testHarness1.testInitialise("xml/config.xml");
      //testHarness1.makePlayers("xml/config.xml");

    }
    catch(Exception e)
    {
      e.printStackTrace();
      Log.logger.severe("Exception - "+e);
    }

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/