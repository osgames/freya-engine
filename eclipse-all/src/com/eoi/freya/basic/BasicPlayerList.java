package com.eoi.freya.basic;

import com.eoi.freya.player.*;
import com.eoi.freya.util.*;
import com.eoi.freya.*;

import org.jdom.*;
import org.jdom.output.*;
import org.jdom.input.*;

import java.util.*;

/**
 * PlayerList is a singleton class that holds all the Players.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicPlayerList {

  protected static BasicPlayerList pdl;

  protected HashMap players;

  protected BasicPlayerList()
  {
    Config config = Config.getInstance();
    players = new HashMap(config.getIntSetting(Def.NUMBER_OF_PLAYERS));
  }

  public static BasicPlayerList getInstance()
  {
    if (pdl == null)
    {
      pdl = new BasicPlayerList();
    }

    return pdl;
  }

  public Player getPlayer(String playerName)
  {

    if (players.containsKey(playerName))
    {
      return (Player)players.get(playerName);
    }
    else
    {
      Player p = new BasicPlayer();

      p.load( playerName);
      players.put(playerName,p);
      return p;
    }
  }

  public void writePlayers()
  {
    Log.logger.info("Players to write..."+players.size());
    Iterator i = players.values().iterator();
    while (i.hasNext())
    {
      ((Player)i.next()).write();
    }
  }

  public void readPlayerList(String filename)
  {
    try
    {

      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(filename);
      Element rootElement = d.getRootElement();

      List l = rootElement.getChildren(Def.PLAYER_FILE);
      ListIterator iterate = l.listIterator();
      while (iterate.hasNext())
      {
        getPlayer(((Element)iterate.next()).getText());
      }
    }
    catch(JDOMException e)
    {
      Log.logger.severe(""+e);
    }
  }

  public HashMap getPlayerHashMap()
  {
    return players;
  }

}
/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/