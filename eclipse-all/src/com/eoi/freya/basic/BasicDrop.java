package com.eoi.freya.basic;

import com.eoi.freya.game.*;
import com.eoi.freya.Log;
import com.eoi.freya.util.SafeCast;
import org.jdom.Element;

/**

 * Description: Handles the drop command.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicDrop extends BasicUnitPhase {

  public BasicDrop()
  {
      command = BDef.DROP;
  }
  public Element executeForUnit(BasicUnit unit, Element commandElement)
  {
    Element result = new Element(BDef.DROP);
    String objectType = commandElement.getChildTextTrim(BDef.TYPE);

    int number = SafeCast.toInt(commandElement.getChildTextTrim(BDef.VALUE));
    if (number<1)
    {
        result.setAttribute("failed","true");
        return result;
    }

    Inventory inventory = unit.getInventory();
    int current = inventory.getItemValue(objectType);

    Log.logger.info(""+unit.getName()+" tries to DROP "+number+" "+objectType);

    // if the location has 1 or items
    if (current != 0)
    {
      // if trying to drop more than unit has, set number to the number available.
      if (number > current)
      {
        number = current;
      }
      // if number = 0, then drop all that is here
      if (number == 0)
      {
        number = current;
      }

      // reduce the units inventory by the amount dropped
      current -= number;
      inventory.setItemValue(objectType,current);

      // get the location of unit.
      Location loc = unit.getLocation();

      // get the inventory of that location and add the item to it
      Inventory mapInventory = loc.getInventory();
      mapInventory.addToItemValue(objectType, number);

      Log.logger.info(""+number+" "+objectType+" dropped.");
      result.setText(""+number+" "+objectType+" dropped");
    }
    else
    {
      result.setAttribute("failed","true");
      result.setText("Nothing to drop");
    }

    return result;

  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/