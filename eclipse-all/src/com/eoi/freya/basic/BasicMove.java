package com.eoi.freya.basic;

import com.eoi.freya.util.*;
import com.eoi.freya.turn.*;
import com.eoi.freya.player.*;
import com.eoi.freya.Log;
import java.util.*;

import org.jdom.*;

/**
 * The Move command passes on movement instructions to units.
 * @author Sasha Bilton
 * @version 1.0
 */
// Move commands are typically <move><name>unit name</name><direction>n,s,e,w</direction><modifier>forced march</modifier></move>

public class BasicMove extends BasicUnitPhase
{

  public BasicMove()
  {
    command = BDef.MOVE;
  }

  public Element executeForUnit(BasicUnit unit, Element moveCommand)
  {
    String directions = moveCommand.getChildText(BDef.DIRECTIONS);
    Log.logger.info("Directions = "+directions);
    if (directions != null && !directions.equals(""))
    {
      return unit.move(moveCommand.getChildText(BDef.DIRECTIONS), moveCommand.getChildText(BDef.MODIFIER));
    }
    else
    {
      Element result = new Element(BDef.MOVE);
      result.setAttribute("failed","true");
      return result;
    }
  }

}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/