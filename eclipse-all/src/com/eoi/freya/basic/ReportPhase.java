package com.eoi.freya.basic;

import com.eoi.freya.*;
import com.eoi.freya.util.*;
import com.eoi.freya.basic.*;
import com.eoi.freya.game.*;
import com.eoi.freya.player.*;
import com.eoi.freya.Log;

import java.util.*;

import org.jdom.Element;

/**
 * Generate reports
 * @author
 * @version 1.0
 */

public class ReportPhase extends BasicPhase {

  public ReportPhase()
  {
    repeat = false;

  }

  public void execute()
  {
    Config c = Config.getInstance();
    String playerList = c.getSetting(Def.PLAYER_LIST);

    // load all the unloaded players
    BasicPlayerList bpl = BasicPlayerList.getInstance();
    bpl.readPlayerList(playerList);

    // Get the report documents
    PlayerDocumentList pdl = PlayerDocumentList.getInstance();

    Iterator i = bpl.getPlayerHashMap().values().iterator();

    while (i.hasNext())
    {
      // get player document
      Player player = (Player)i.next();
      PlayerDocument pd = pdl.getPlayerDocument(player.getName());

      BasicMap map = BasicMap.getInstance();

      Iterator u = player.getUnits();
      while (u.hasNext())
      {
        BasicUnit unit = (BasicUnit)u.next();
        BasicPosition pos = unit.getPosition();

        int visualRange = 3;

        try
        {
          visualRange = unit.getIntSetting(BDef.VISUAL_RANGE);
        }
        catch(NoSuchElementException nsee)
        {
        }

        Log.logger.info("get map portion for "+unit.getName());
        Element view = map.getMapPortion(pos.getX()-visualRange, pos.getY()-visualRange, visualRange * 2, visualRange * 2 );

        Element result = pd.getResultForUnit(unit.getName());

        result.addContent(view);

        Element unitResult = (Element)unit.getInfo().clone();
        unitResult.setName("unit-result");

        Element stats = new Element(BDef.STATS);

        stats.addContent(unitResult);

        result.addContent(stats);

      }

    }

  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/