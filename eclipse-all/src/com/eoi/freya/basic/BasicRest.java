package com.eoi.freya.basic;

import com.eoi.freya.Log;
import com.eoi.freya.turn.*;
import com.eoi.freya.player.*;
import com.eoi.freya.game.*;

import java.util.*;

import org.jdom.*;

/**
 * The rest command heals units that are capable of rest a percentage of the max health.
 * This rate is defined in GameConfig file.
 * TODO : Move the rate to the units DataDictionary file.
 * Copyright:    Ends Of Invention Copyright (c) 2001
 * Company:      www.endsofinvention.com
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicRest extends BasicUnitPhase
{

  public BasicRest()
  {
    command = BDef.REST;
  }

  public Element executeForUnit(BasicUnit unit, Element restCommand)
  {
    Element result = new Element(BDef.REST);

    // Robots can't rest, units that have attacked or moved can't rest.
    if (!unit.isSetting(BDef.REST) || !unit.hasAttacked || !unit.hasMoved)
    {
      BasicHealth health = unit.getHealth();

      GameConfig gc = GameConfig.getInstance();
      double healPercent = Double.parseDouble(gc.getSetting(BDef.HEAL_RATE));

      int heal = (int)(health.getMax() * healPercent);
      health.add(heal);
      result.setText("rested and healed "+heal+" health points.");
    }
    else
    {
      result.setAttribute("failed","true");
      result.setText("failed");
    }
    return result;

  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
