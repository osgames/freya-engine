package com.eoi.freya.basic;

import com.eoi.freya.turn.*;

import com.eoi.freya.Log;

import org.jdom.Element;

import java.util.*;

/**
 * Modifiers are duration based effects that usually change attributes.
 * @author
 * @version 1.0
 */

/*Basic modifiers look like this in XML
 * <modifier>
 *  <type>basic</type>
 *  <counter>10</counter>
 *  <attribute>attack</attribute>
 *  <value>-2</value>
 * </modifier>
 */
public class RemoveModifiers extends BasePhase
{


  public void execute()
  {
    // get the Player object for this player
    player = BasicPlayerList.getInstance().getPlayer(playerCommands.getPlayerName());

    // get it's units
    Iterator units = player.getUnits();

    // foreach unit
    while( units.hasNext())
    {
      BasicUnit unit = (BasicUnit)units.next();

      // get each modifier
      Iterator mods = unit.getModifiers().iterator();

      // foreach modifier
      while(mods.hasNext())
      {

        Element mod = (Element)mods.next();

        // if the modifier is a basic type
        if (mod.getChildText(BDef.TYPE).equals(BDef.BASIC))
        {
          removeBasicMods(mod, unit);
        }
      }
    }
  }

  /** This method will remove a BASIC modifier from a unit*/
  protected void removeBasicMods(Element mod, BasicUnit unit)
  {
   // get the turn counter
          BasicValue counter = new BasicValue(mod.getChild(BDef.COUNTER));

          // and subtract one from it
          counter.subtract(1);

          // if it is 0 or less
          if (counter.getValue() < 1)
          {
            // get the attribute it affects
            String attribute = mod.getChildTextTrim(BDef.ATTRIBUTE);

            // and the value to add to the attribute
            int value = Integer.parseInt( mod.getChildTextTrim(BDef.VALUE));

            // get the attribute, as a BasicValue
            BasicValue attributeToModify = new BasicValue(unit.getElement(attribute));

            //modify the attribute
            attributeToModify.add(value);

            // delete the modifier from the unit
            mod.detach();
            mod = null;
          }
    }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/