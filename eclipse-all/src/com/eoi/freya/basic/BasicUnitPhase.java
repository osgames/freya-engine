package com.eoi.freya.basic;

import com.eoi.freya.turn.*;
import com.eoi.freya.player.*;
import com.eoi.freya.Log;

import java.util.*;

import org.jdom.*;

/**
 * BasicUnitPhase is a base class for Commands that work at a unit level.
 * Child classes implement the executeForUnit(..) method that already has identified the Unit performing the command.
 * @author Sasha Bilton
 * @version 1.0
 */

public abstract class BasicUnitPhase extends BasicPhase
{

  public String command = BDef.NOT_DEFINED;

  public String getCommand()
  {
    return command;
  }

  public void setCommand(String command)
  {
    this.command = command;
  }

  public void execute()
  {
    // get the Player object for this player
    Player player = getPlayer();
    Log.logger.info(player.getName()+" "+getCommand());
    List l = getCommands(getCommand());
    Iterator i = l.iterator();
    PlayerDocument pd = getPlayerDocument();
    while(i.hasNext())
    {
      Element commandElement = (Element)i.next();

      String unitName = commandElement.getChildTextTrim(BDef.NAME);

      BasicUnit unit = (BasicUnit)player.getUnit(unitName);

      if (unit != null && !unit.isDead())
      {
        Element result = executeForUnit(unit, commandElement);
        if (result != null)
        {
          // detach just to be sure
          result.detach();
          Element unitResult = pd.getResultForUnit(unit.getName());
          unitResult.addContent(result);
        }
      }
    }
  }

  public abstract Element executeForUnit(BasicUnit unit, Element commandElement);
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/