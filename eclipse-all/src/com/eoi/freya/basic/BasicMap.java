package com.eoi.freya.basic;

import com.eoi.freya.Def;
import com.eoi.freya.util.Config;
import com.eoi.freya.game.*;
import com.eoi.freya.Log;

import java.util.*;
import java.io.FileOutputStream;

import org.jdom.*;
import org.jdom.output.XMLOutputter;
import org.jdom.input.SAXBuilder;
/**
 * The BasicMap is a huge 2D array of BasicLocations
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicMap implements com.eoi.freya.game.Map
{
  private BasicLocation[][] map;

  protected static BasicMap mapInstance;
  private int max_x = 0;
  private int max_y = 0;


  protected BasicMap()
  {
    Config c = Config.getInstance();
    max_y = c.getIntSetting(Def.MAX_X);
    max_x = c.getIntSetting(Def.MAX_Y);


    map = new BasicLocation[max_x][max_y];
  }

  public static BasicMap getInstance()
  {
    if (mapInstance == null)
    {
      mapInstance = new BasicMap();
    }

    return mapInstance;
  }

  /** Given a root element createFromElement fills the map array with the XML data*/
  public void createFromElement(Element mapElement)
  {

    Log.logger.info("root map element = "+mapElement);

    Iterator i = mapElement.getChildren(BDef.ROW).iterator();
    while(i.hasNext())
    {
      Element row = (Element)i.next();
      Iterator ii = row.getChildren(BDef.LOCATION).iterator();
      while(ii.hasNext())
      {
        Element loc = (Element)ii.next();

        if (loc.getChild(BDef.INVENTORY) == null)
        {
          loc.addContent(new Element(BDef.INVENTORY));
        }


         BasicLocation location = new BasicLocation(loc);
        int x = location.getX();
        int y = location.getY();

        map[x][y] = location;
      }
    }

  }

  /** Converts entire map to an element, filling missing xy's with a blank element.*/
  public Element convertToElement()
  {
    Element returnElement = new Element(BDef.MAP);

    for(int y=0; y<max_y;y++)
    {
      Element row = new Element(BDef.ROW);
      row.setAttribute(BDef.Y, String.valueOf(y));

      for(int x=0; x<max_x;x++)
      {
        BasicLocation location = map[x][y];
        if (location == null)
        {
          location = new BasicLocation();

          location.setX(x);
          location.setY(y);

          location.setTerrain(BDef.NOT_DEFINED);
          map[x][y] = location;
          location.getElement().detach();
        }
        else
        {
          location = new BasicLocation ((Element)location.getElement().clone());
          location.getElement().detach();
        }


        row.addContent(location.getElement());
      }

      returnElement.addContent(row);
    }
    return returnElement;
  }

  /** Creates a root element that contains a portion of map as XML*/
  public Element getMapPortion(int x, int y, int width, int height)
  {
    Element returnElement = new Element(BDef.MAP);

    int end_x = x+width+1>max_x ? max_x : x+width+1;
    int end_y = y+height+1>max_x ? max_y : y+height+1;
    x = x < 0 ? 0 : x;
    y = y < 0 ? 0 : y;

    for(int ly=y; ly<end_y;ly++)
    {
      Element row = new Element(BDef.ROW);
      row.setAttribute(BDef.Y, String.valueOf(ly));


      for(int lx=x; lx<end_x;lx++)
      {

        BasicLocation location = map[lx][ly];
        if (location == null)
        {
          location = new BasicLocation();

          location.setX(lx);
          location.setY(ly);

          location.setTerrain(BDef.NOT_DEFINED);
        }

        row.addContent((Element)(location.getElement().clone()));
      }

      returnElement.addContent(row);
    }
    return returnElement;
  }

  /** get the Location object of a give x y spot*/
  public Location getLocation(int x, int y)
  {
    return map[x][y];
  }

  /** get the Location object of a give BasicPosition*/
  public Location getLocation(BasicPosition pos)
  {
    return map[pos.getX()][pos.getY()];
  }

  /** return true is Location has a give unit at a given x y spot*/
  public boolean hasUnit(String name, BasicPosition pos)
  {
    return map[pos.getX()][pos.getY()].hasUnit(name);
  }

  /** return true is Location has a give unit at a given BasicPosition*/
  public boolean hasUnit(String name, int x, int y)
  {
    return map[x][y].hasUnit(name);
  }

  /** write the map to disk*/
  public void write(String filename)
  {
    try
    {

      Log.logger.info("Writing "+filename);
      FileOutputStream fos = new FileOutputStream(filename);

      XMLOutputter xo = new XMLOutputter("   ",true);
      xo.setTextNormalize(true);
      Element mapElement = this.convertToElement();
      Document mapDoc = new Document(mapElement);

      xo.output(mapDoc,fos);
      fos.flush();
      fos.close();
    }
    catch (Exception e)
    {
      Log.logger.severe("Player failed to write "+filename+" due to "+e);
    }

  }

  /** read the map from disk*/
  public void read(String filename)
  {
    try
    {
    SAXBuilder builder = new SAXBuilder();
    Log.logger.info("Loading map file "+filename);
    Document mapDoc  = builder.build(filename);
    createFromElement(mapDoc.getRootElement());
    }
    catch(Exception e)
    {
      Log.logger.severe(" load map failed "+e);
    }
  }

  public void setMap(BasicLocation[][] newMap)
  {
    map = newMap;
  }

  public int getMaxX()
  {
    return max_x;
  }

  public int getMaxY()
  {
    return max_y;
  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
