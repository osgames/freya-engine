package com.eoi.freya.basic;

import com.eoi.freya.game.*;
import com.eoi.freya.Log;
import org.jdom.Element;

/**
 * BasicMutate will give a Unit the same basic values as that of another, which has a less cost.
 * UNTESTED AT THE MOMENT
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicMutate extends BasicUnitPhase {

  public BasicMutate()
  {
      command = BDef.MUTATE;
  }
  public Element executeForUnit(BasicUnit unit, Element commandElement)
  {
    Element result = new Element(BDef.MUTATE);

    //see if unit can mutate
    if (!unit.isSetting(BDef.MUTATE))
    {
      result.setAttribute("failed","true");
      result.setText("Unit can't Mutate");
    }
    else
    {
      //get the cost value of the mutator
      int cost = unit.getCost().getValue();

      //get the type of the new 'shape'
      String unitType = commandElement.getChildTextTrim(BDef.TYPE);

      //get the definition of the new shape (currently you can't change race)
      Element newUnitDef = (Element)(DataDictionary.getInstance().get("creature/"+unit.getRace()+"/"+unitType+"/unit")).clone();
      newUnitDef.detach();

      //get the cost of the new shape
      int newCost = Integer.parseInt(newUnitDef.getChild(BDef.COST).getChildText(BDef.VALUE));

      // see if the cost of the mutator is more than the cost of the new shape
      if (cost > newCost)
      {
        //Now make an BasicUnit out of the info block of the new shape
        StandardUnit newShape = new StandardUnit(newUnitDef);

        //copy the new shapes values to the unit, NOTE remember these are references not values
        BasicAttack attack = unit.getAttack();
        attack = newShape.getAttack();
        BasicDefence defence = unit.getDefence();
        defence = newShape.getDefence();
        BasicHealth health = unit.getHealth();
        health = newShape.getHealth();
        BasicValue move = unit.getMovementPoints();
        move = newShape.getMovementPoints();

      }
      else
      {
        result.setAttribute("failed","true");
        result.setText("New shape costs too much");

      }

    }
    return result;

  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/