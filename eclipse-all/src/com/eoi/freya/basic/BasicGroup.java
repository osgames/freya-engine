package com.eoi.freya.basic;

import com.eoi.freya.game.*;

import org.jdom.Element;

import java.util.*;
/**
 * A Group is a collection of units that can be treated as a single entity.
 * TODO : Make Group implement Unit so that it can be applied seemlessly?
 * @author Sasha Bilton
 * @version 1.0
 */

 // Groups follows <group><name>1st army</name><owner>bob</owner><unit>alpha</unit><unit>beta</unit></group>

public class BasicGroup extends OwnedObject
{

  public BasicGroup(Element info)
  {
    super(info);
  }

  public boolean isUnitInGroup(String unitName)
  {
    Iterator i = getUnits();
    while(i.hasNext())
    {
      Element unit = (Element)i.next();
      if (unit.getTextTrim().equals(unitName))
      {
        return true;
      }
    }
    return false;

  }

  public void addUnit(String unitName)
  {
    if(!isUnitInGroup(unitName))
    {
      Element unit = new Element(BDef.UNIT);
      unit.setText(unitName);
      info.addContent(unit);
    }

  }

  public Iterator getUnits()
  {
    return info.getChildren(BDef.UNIT).iterator();
  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/