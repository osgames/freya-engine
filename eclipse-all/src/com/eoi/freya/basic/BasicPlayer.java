package com.eoi.freya.basic;

import com.eoi.freya.player.Player;
import com.eoi.freya.game.*;
import com.eoi.freya.util.*;
import com.eoi.freya.Def;
import com.eoi.freya.Log;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import java.util.*;
import java.io.*;

/**
 * A BasicPlayer Object represents an in-game player and is a container for all the data that
 * player has. Units and Groups make up the majority of this. A BasicPlayer can persist itself.
 * @author Sasha Bilton
 * @version 1.0
 */


public class BasicPlayer extends XMLObject implements Player
{

  protected Document playerDoc;
  protected HashMap units;
  protected HashMap groups;

  public BasicPlayer()
  {
     super(new Element("player"));

    playerDoc = new Document(info);
    //ProcessingInstruction pi = new ProcessingInstruction("<?xml-stylesheet type=\"text/xsl\"", "href=\"xml\\player-out.xsl\"?>");
    //playerDoc.addContent(pi);

    info.addContent(new Element(BDef.NAME));
    info.addContent(new Element(BDef.UNITS_LIST));
    info.addContent(new Element(BDef.GROUPS_LIST));
    info.addContent(new Element(BDef.RACE));

    units = new HashMap();
    groups = new HashMap();
  }

  public BasicPlayer(Element info)
  {
    super(info);
    units = new HashMap();
    groups = new HashMap();
    playerDoc = info.getDocument();
    Log.logger.info(getName()+" created");
    System.out.println(getName()+" created");
  }


  public String getFilename()
  {
    String directory = Config.getInstance().getSetting(Def.PLAYER_FILE_OUT_DIR);
    return directory + getSetting(BDef.NAME) + ".xml";
  }

  public Unit getUnit(String unitName)
  {
    return (BasicUnit)units.get(unitName);
  }

  public String getRace()
  {
    return info.getChildTextTrim(BDef.RACE);
  }

  public void setRace(String race)
  {
    info.getChild(BDef.RACE).setText(race);
  }

  public Iterator getUnits()
  {
    return units.values().iterator();
  }

  public BasicGroup getGroup(String unitName)
  {
    return (BasicGroup)groups.get(unitName);
  }

  public Iterator getGroups()
  {
    return groups.values().iterator();
  }

  public void addUnit(BasicUnit unit)
  {
    if (!units.containsKey(unit.getName()))
    {
      units.put(unit.getName(), unit);
      getElement(BDef.UNITS_LIST).addContent(unit.getInfo());
    }
    else
    {
      Log.logger.warning("Unit "+unit.getName()+" already exists, when trying to BasicPlayer.addUnit()");
    }

  }

  public void addGroup(BasicGroup group)
  {
    if (!groups.containsKey(group.getName()))
    {
      groups.put(group.getName(), group);
      getElement(BDef.GROUPS_LIST).addContent(group.getInfo());
    }
        else
    {
      Log.logger.warning("Group "+group.getName()+" already exists, when trying to BasicPlayer.addGroup()");
    }
  }

  public void buildUnits()
  {

      Iterator i = getElement(BDef.UNITS_LIST).getChildren(BDef.UNIT).iterator();
      BasicUnitList bul = BasicUnitList.getInstance();

      while (i.hasNext())
      {
        Element unitInfo = (Element)i.next();
        BasicUnit unit =  (BasicUnit)UnitFactory.createNewUnit(unitInfo.getChildTextTrim(BDef.RACE), unitInfo.getChildTextTrim(BDef.TYPE), unitInfo);
        units.put(unit.getName(),unit);
        bul.addUnit(unit);
        BasicMap map = BasicMap.getInstance();
        Location loc = map.getLocation(unit.getPosition());
        Log.logger.info("loc = "+loc+" ux = "+unit.getPosition().getX()+" uy = "+unit.getPosition().getY());

        loc.addUnit(unit);

      }
  }

  public void buildGroups()
  {
    Iterator i = getElement(BDef.GROUPS_LIST).getChildren(BDef.GROUP).iterator();
    while (i.hasNext())
    {
      Element groupInfo = (Element)i.next();
      BasicGroup group = new BasicGroup(groupInfo);
      groups.put(group.getName(), group);
    }
  }

  public void load(String name)
  {
    try
    {
      String directory = Config.getInstance().getSetting(Def.PLAYER_FILE_DIR);
      SAXBuilder builder = new SAXBuilder();

      String filename = directory + name + ".xml";
      Log.logger.info("Loading player file "+filename);

      playerDoc = builder.build(filename);
      info = playerDoc.getRootElement();

      buildUnits();
      buildGroups();

    }
    catch (Exception e)
    {
      Log.logger.severe("Player failed to load "+name+" due to "+e);
      e.printStackTrace();
    }

  }
  public void write()
  {
    try
    {

      Log.logger.info("Writing "+getName());
      FileOutputStream fos = new FileOutputStream(getFilename());

      XMLOutputter xo = new XMLOutputter("   ",true);
      xo.setTextNormalize(true);

      xo.output(playerDoc,fos);
    
  //    Log.logger.info(xo.outputString(playerDoc));


      fos.flush();
      fos.close();
    }
    catch (Exception e)
    {
      Log.logger.severe("Player failed to write "+getFilename()+" due to "+e);
    }

  }


}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
