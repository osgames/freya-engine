package com.eoi.freya.basic;

import org.jdom.Element;

import com.eoi.freya.Log;

/**
 * BasicPosition holds a JDOM XML Element that contains 2D (x,y) positional data
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicPosition
{
  protected Element values;

  public BasicPosition(Element values)
  {
    this.values = values;
  }

  public BasicPosition(int x, int y)
  {
    values = new Element(BDef.POSITION);
    setXY(x,y);
  }
  public int getX()
  {
    return Integer.parseInt(  values.getAttributeValue(BDef.X));
  }


  public int getY()
  {
    return Integer.parseInt(  values.getAttributeValue(BDef.Y));
  }

  public void setXY(int x, int y)
  {
    setX(x);
    setY(y);
  }

  public void setX(int x)
  {
    values.setAttribute(BDef.X,""+x);
  }

  public void setY(int y)
  {
    values.setAttribute(BDef.Y,""+y);
  }

  public Element getElement()
  {
    return values;
  }

  public boolean equals(BasicPosition p)
  {
    return (p.getX() == getX() && p.getY() == getY());
  }

  public String toString()
  {
    return "["+getX()+"/"+getY()+"]";
  }

}
/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
