package com.eoi.freya.game;

import com.eoi.freya.Log;

import org.jdom.*;
import org.jdom.input.*;

import java.util.*;
/**
 * Global Game Config data handler
 * @author
 * @version 1.0
 */

public class GameConfig {

  private String filename;
  private Element rootElement;

  private static GameConfig config = null;

  protected GameConfig(String filename)
  {
    try
    {
      this.filename = filename;
      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(filename);
      rootElement = d.getRootElement();

      builder = null;
      d = null;
    }
    catch(JDOMException ex)
    {
      Log.logger.severe("GameConfig - "+ex);
    }
  }

  public static GameConfig loadFromFile(String filename)
  {
    if (config == null)
    {
      config = new GameConfig(filename);
    }
    return config;
  }

  public static GameConfig getInstance() throws NullPointerException
  {
    if (config == null)
    {
      throw new NullPointerException("Call to Config.getInstance before loaded");
    }
    else
    {
      return config;
    }
  }

  public String getSetting(String setting) throws NoSuchElementException
  {
    Element e = rootElement.getChild(setting);
    if (e == null)
    {
      throw new NoSuchElementException(setting+" not found in "+filename);
    }
    else
    {
      return e.getTextTrim();
    }
  }

    public int getIntSetting(String setting) throws NoSuchElementException
  {
    Element e = rootElement.getChild(setting);
    if (e == null)
    {
      throw new NoSuchElementException(setting+" not found in "+filename);
    }
    else
    {
      return Integer.parseInt(  e.getTextTrim());
    }
  }

  public Element getElement(String name)
  {
    Element e = rootElement.getChild(name);
    if (e == null)
    {
      throw new NoSuchElementException(name+" not found in "+filename);
    }
    else
    {
      return e;
    }

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/