package com.eoi.freya.game;

import com.eoi.freya.Def;
import org.jdom.*;
/**
 * An Item wraps up an Element that contains data to describe generic items.
 * Each item has a 'TYPE' and a 'VALUE', which is usually used to represent quantity.
 * @author Sasha Bilton
 * @version 0.1
 */
public class Item {

/**
 * The XML element that holds the item data.
 */
  private Element itemElement=null;

  /**
   * Construct an empty Item object with no contents for the TYPE and VALUE tags
   */
  public Item()
  {
    itemElement = new Element(Def.ITEM);
    itemElement.addContent(new Element(Def.TYPE));
    itemElement.addContent(new Element(Def.VALUE));
  }

  /**
   * Construct and item using an existing XML Element
   * @param value The Element to use.
   */
  public Item(Element value)
  {
    itemElement = value;
  }


  /**
   * @return The String value of the type of the item.
   */
  public String getType()
  {
    return itemElement.getChildText(Def.TYPE);
  }

  /**
   * @param newType Set the type of this item.
   */
  public void setType(String newType)
  {
    itemElement.getChild(Def.TYPE).setText(newType);
  }

  /**
   * @return the int quantity of the item .
   */
  public int getValue()
  {
    return Integer.parseInt(  itemElement.getChildTextTrim(Def.VALUE));
  }

  /**
   * @param value Set int quantity of the item.
   */
  public void setValue(int value)
  {
    itemElement.getChild(Def.VALUE).setText(""+value);
  }

  /**
   * @param value Set the String quantity of the item.
   */
  public void setValue(String value)
  {
    itemElement.getChild(Def.VALUE).setText(value);
  }

  /**
   * @param addValue add a quantity to the current value
   */
  public void add(int addValue)
  {
    setValue( getValue() + addValue );
  }

  /**
   * @param subValue the int value to subtract from the current amount
   */
  public void subtract(int subValue)
  {
    setValue( getValue() - subValue );
  }

  /**
   * @return the XML Element of this Item
   */
  public Element getElement()
  {
    return itemElement;
  }

  /**
   * @param value
   */

  public void setElement(Element value)
  {
    itemElement = value;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/