package com.eoi.freya.game;

import com.eoi.freya.Log;

import org.jdom.*;
import org.jdom.input.*;

import java.util.*;

/**
 * DataDictionary is a base class for holding dictionary data.
 * @author Sasha Bilton
 * @version 1.0
 */


public class DataDictionary
{
  protected String filename;
  protected Element rootElement;

  private static DataDictionary instance;

  protected DataDictionary(String filename)
  {
    try
    {
      Log.logger.info("Loading Data Diction : "+filename);
      this.filename = filename;
      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(filename);
      rootElement = d.getRootElement();

      builder = null;
      d = null;

    }
    catch(JDOMException ex)
    {
      System.out.println(ex);
      Log.logger.severe("DataDictionary - "+ex);
    }
  }

  public static DataDictionary loadDataDictionaryList(String filename)
  {
    if (instance == null)
    {
      instance = new DataDictionary(filename);
    }
    return instance;
  }

  public static DataDictionary getInstance() throws NullPointerException
  {
    if (instance == null)
    {
      throw new NullPointerException("DataDictionary.getInstance called before loading");
    }
    else
    {
      return instance;
    }
  }

  //Eventually use XPath
  // at the moment element1/element2/etc
  public Element get(String name)
  {
    Log.logger.info("getting "+ name);
    Element current = rootElement;
    StringTokenizer elementNames = new StringTokenizer(name,"/");
    while(elementNames.hasMoreTokens())
    {

      String elementName = elementNames.nextToken();
      current = current.getChild(elementName);
    }

    return current;

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/