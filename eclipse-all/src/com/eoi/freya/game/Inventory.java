
package com.eoi.freya.game;

import com.eoi.freya.Def;
import com.eoi.freya.Log;

import org.jdom.*;

import java.util.*;

/**
 * An Inventory is an JDOM XML Element based object that stores a list of items
 * @author Sasha Bilton
 * @version 1.0
 */


public class Inventory
{
  protected Element items;

  public Inventory()
  {
    items = new Element(Def.INVENTORY);
  }

  public Inventory(Element items)
  {
    this.items = items;
  }

  public Element getItems()
  {
    return items;
  }


  public Element getItemElement(String type)
  {
    try
    {
      Element result = null;
      Iterator itemList = items.getChildren(Def.ITEM).iterator();
      while (itemList.hasNext() && result == null)
      {
         Element current = (Element)itemList.next();
         if (current.getChildText(Def.TYPE).equals(type))
         {
            result = current;
         }
      }
      return result;
    }
    catch(NullPointerException npe)
    {
      return null;
    }
  }

  public void addItem(Item toAdd)
  {
    addItem(toAdd.getElement());
  }

  public Item getItem(String type)
  {
    Element itemElement = getItemElement(type);
    if (itemElement != null)
    {
      return new Item(itemElement);
    }
    else
    {
      return null;
    }
  }

  public void addItem(Element toAdd)
  {
    String type = toAdd.getChildText(Def.TYPE);
    Element alreadyExists = getItemElement(type);

    if(alreadyExists != null)
    {
      int newValue = getItemValue(type) + Integer.parseInt(toAdd.getChildText(Def.VALUE));
      Element value = alreadyExists.getChild(Def.VALUE);
      value.setText(String.valueOf(newValue));
    }
    else
    {
      toAdd.detach();
      items.addContent(toAdd);
    }

  }

  public int getItemValue(String type)
  {
    try
    {
      String value = getItemElement(type).getChildText(Def.VALUE);
      if (value.equals(""))
      {
        value = "0";
      }
      return Integer.parseInt(value.trim());
    }
    catch(NullPointerException npe)
    {
      //Log.logger.info("No such Item.");
      return 0;
    }

  }

  public void addItemValue(String type, int value)
  {
    Element toAdd = new Element(Def.ITEM);
    toAdd.addContent(new Element(Def.TYPE).setText(type));
    toAdd.addContent(new Element(Def.VALUE).setText(String.valueOf(value)));
    addItem(toAdd);
  }

  public void setItemValue(String type, int value)
  {
    Element toSet = getItemElement(type).getChild(Def.VALUE);
    if (toSet != null)
    {
        toSet.setText(String.valueOf(value));
    }
    else
    {
      addItemValue(type,value);
    }
  }

  public void addToItemValue(String type, int value)
  {
    Element toSet = getItemElement(type);
    if (toSet != null)
    {
      toSet = toSet.getChild(Def.VALUE);
      int current = this.getItemValue(type);
      current += value;
      toSet.setText(String.valueOf(current));
    }
    else
    {
      addItemValue(type,value);
    }

  }

  public void decreaseFromItemValue(String type, int value)
  {
    try
    {
        Element toSet = getItemElement(type).getChild(Def.VALUE);
        if (toSet != null)
        {
            int current = this.getItemValue(type);
            current -= value;
            if (current < -0)
            {
              current = 0;
            }
            toSet.setText(String.valueOf(current));
        }
    }
    catch(NullPointerException npe)
    {
      //Log.logger.warning("Decreasing inventory when the inventory doesn't have the object.");
    }
  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/