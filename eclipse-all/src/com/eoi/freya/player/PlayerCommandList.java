package com.eoi.freya.player;

import com.eoi.freya.*;
import com.eoi.freya.util.*;

import org.jdom.*;
import org.jdom.input.*;


import java.util.*;
/**
 * This class is a singleton that loads and maintains all the players command lists. It also iterators through the list.
 * @author Sasha Bilton
 * @version 1.0
 */
public class PlayerCommandList {

  private static PlayerCommandList players = null;

  private ArrayList playerList = new ArrayList();

  private int currentPlayer = 0;

  protected PlayerCommandList(String filename)
  {
    try
    {
      Log.logger.info("loading Command list "+filename);
      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(filename);
      Element rootElement = d.getRootElement();
      List l = rootElement.getChildren(Def.PLAYER_FILE);
      ListIterator iterate = l.listIterator();

      while (iterate.hasNext())
      {
        String playerFile = (String)((Element)iterate.next()).getTextTrim();
        PlayerCommands pc = new PlayerCommands(playerFile);
        playerList.add(pc);
        Log.logger.info(playerFile+" loaded");
      }

    }
    catch(JDOMException ex)
    {
      Log.logger.severe("Config - "+ex);
    }
  }

  public boolean nextPlayer()
  {
    return (currentPlayer <= playerList.size()-1);
  }

  public PlayerCommands getNextPlayer()
  {
    return (PlayerCommands)playerList.get(currentPlayer++);
  }


  public static PlayerCommandList loadPlayerList(String filename)
  {
    if (players == null)
    {
      players = new PlayerCommandList(filename);
    }
    return players;
  }

  public static PlayerCommandList getInstance() throws NullPointerException
  {
    if (players == null)
    {
      throw new NullPointerException("PlayerList.getInstance called before loading");
    }
    else
    {
      return players;
    }
  }

  public void reset()
  {
    currentPlayer = 0;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/