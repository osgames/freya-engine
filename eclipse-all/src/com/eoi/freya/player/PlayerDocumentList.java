package com.eoi.freya.player;

import com.eoi.freya.util.*;
import com.eoi.freya.*;

import org.jdom.*;
import org.jdom.output.*;

import java.util.*;

/**
 * PlayerDocumentList is a singleton class that holds all the PlayerDocuments.
 * @author Sasha Bilton
 * @version 1.0
 */

public class PlayerDocumentList {

  private static PlayerDocumentList pdl;

  private HashMap playerDocs;
  private Iterator playerList;



  protected PlayerDocumentList()
  {
    Config config = Config.getInstance();
    playerDocs = new HashMap(config.getIntSetting(Def.NUMBER_OF_PLAYERS));
    playerList = playerDocs.values().iterator();
  }

  public static PlayerDocumentList getInstance()
  {
    if (pdl == null)
    {
      pdl = new PlayerDocumentList();
    }
    return pdl;
  }

  public boolean nextPlayer()
  {
    return playerList.hasNext();
  }

  public PlayerDocument getNextPlayer()
  {
    if (playerList.hasNext())
    {
      return (PlayerDocument)playerList.next();
    }
    else
    {
      return null;
    }
  }

  public void resetPlayerList()
  {
    playerList = playerDocs.values().iterator();
  }

  public PlayerDocument getPlayerDocument(String playerName)
  {

    if (playerDocs.containsKey(playerName))
    {
      return (PlayerDocument)playerDocs.get(playerName);
    }
    else
    {
      PlayerDocument pd = new PlayerDocument(playerName);
      playerDocs.put(playerName,pd);
      return pd;
    }
  }

  public void writePlayerDocs()
  {
    Iterator i = playerDocs.values().iterator();
    while (i.hasNext())
    {
      ((PlayerDocument)i.next()).writeDoc();
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/