package com.eoi.freya.player;

import com.eoi.freya.*;
import com.eoi.freya.util.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

import java.util.Iterator;
import java.io.*;

import org.jdom.transform.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

/**
 * This class holds a Document object for each player in a game. The Document is the basis of the report the player interacts with.
 * @author Sasha Bilton
 * @version 1.0
 */

public class PlayerDocument {

  protected Document doc;
  protected Element root;
  private String directory;
  //** Create a new PlayerDocument named using a player name*/
  public PlayerDocument(String playerName)
  {
    Config config = Config.getInstance();
    directory = config.getSetting(Def.PLAYER_DOC_DIR);

    root = new Element(Def.PLAYER_DOC);
    Element player = new Element(Def.PLAYER_NAME);
    player.setText(playerName);
    root.addContent(player);
    doc = new Document(root);
  }

  public String getPlayerName()
  {
    return root.getChildTextTrim(Def.PLAYER_NAME);
  }

  public void write(Element element)
  {
    if (element !=null)
    {
      root.addContent(element);
    }
  }

  public Element getResultForUnit(String name)
  {
    Element result = null;
    boolean notFound = true;
    Iterator units = root.getChildren(Def.UNIT).iterator();
    while (units.hasNext() && notFound)
    {
      Element unit = (Element)units.next();
      if (unit.getChildTextTrim(Def.NAME).equals(name))
      {
        result = unit;
        notFound = false;
      }
    }
    if (notFound) // create a new area for this units results
    {
      Element unit = new Element(Def.UNIT);
      Element unitName = new Element(Def.NAME);
      unitName.setText(name);
      unit.addContent(unitName);
      root.addContent(unit);
      result = unit;

    }
    return result;
  }

  public void addResult(String name, Element element)
  {
    if (element !=null && name!=null)
    {
      Element sub = root.getChild(name);
      if (sub == null)
      {
        sub = new Element(name);
        root.addContent(sub);
      }
      sub.addContent(element);
    }
  }





  public void writeDoc()
  {
    try
    {
      FileOutputStream fos = new FileOutputStream(directory +"//"+ (getPlayerName().replace(' ','_'))+".xml");
      FileOutputStream html = new FileOutputStream(directory +"//"+ (getPlayerName().replace(' ','_'))+".html");

      XMLOutputter xo = new XMLOutputter(" ",true);

      Document htmlDoc = transform(Config.getInstance().getSetting(Def.XSL_FILE));

      xo.output(doc,fos);

      xo.output(htmlDoc,html);
      fos.flush();
      fos.close();
    }
    catch (Exception e)
    {
      Log.logger.severe("PlayerDocument failed to write "+getPlayerName()+" due to "+e);
    }
  }

 public Document transform(String stylesheet)
 {
    try {
      Transformer transformer = TransformerFactory.newInstance()
        .newTransformer(new StreamSource(stylesheet));

      JDOMResult out = new JDOMResult();
      transformer.transform(new JDOMSource(doc), out);
      return out.getDocument();
    }
    catch (Exception e) {
      Log.logger.severe("Tranform failed : "+e);
      return null;
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/