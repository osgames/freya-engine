package com.eoi.freya;

import java.util.logging.*;
import com.eoi.freya.util.Logger;

/**
 *  The Log class works as a proxy to the loggering method used. If running under JDK1.4
 * or using Lumberjack (see http://sourceforge.net/projects/lumberjack/) use the first creator. If using
 * pre JDK 1.4, use the second.
 * @author Sasha Bilton
 * @version 1.0
 */

public class Log {

  //JDK1.4 or Lumberjack logging
  public static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("com.freya.util.Log");

  //JDK1.3 or less, uses very simple System.out logging.
  //public static com.eoi.freya.util.Logger logger = new com.eoi.freya.util.Logger();
  static
  {
    logger.info("Logger started");
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/