#!/bin/bash

export CLASSPATH="/var/jdom-b8/build/jdom.jar:/var/jdom-b8/lib/crimson.jar:/var/jdom-b8/lib/jaxp.jar:/var/jdom-b8/xalan.jar:/var/jdom-b8/lib/xerces.jar:/var/jakarta-ant-1.4.1/lib/ant.jar:/home/admin/freya-working/dist/lib/Freya-latest.jar"

export PATH="$PATH:/usr/java/j2sdk1.4.0/bin"

cd freya-working
java com.eoi.freya.crystalcaverns.util.GenerateMap xml\config.xml
