This is the readme file for the Freya-Engine project

1. What is Freya-Engine?

It's a Java API for writing multiplayer turn based games. It includes a sample game "Crystal Caverns".

2. What isn't Freya-Engine?

It's not a GUI toolkit. It produces XML for output, which can be then be transformed into whatever you want. It's not real-time.

3. How do I run it...
(Assuming you have Java installed - 1.4+ recommended)
 3.1 Get the latest entire release from source-forge. 
 3.2 Get JDOM from www.jdom.org and make sure your classpath points to the jdom.jar
 3.3 Either roll your own build using Ant (see the build.xml file in your freya-engine directory) or use the prebuilt jar 
 3.4 Point you classpath at the latest freya jar build in freya/dist/lib
 3.5 run the command java com.eoi.freya.Game freya/xml/config.xml (where freya/xml/ is the path to config.xml)
 3.6 You should see a longish info and debug list of log messages. If no Exceptions occured, you done it.

4. What now?
Start by looking at the xml files, particularly the command files and the files in 'out' and 'player-out' dierctories.
Have a lot of fun!
 
 