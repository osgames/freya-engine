package com.eoi.freya.basic;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.game.Inventory;
import com.eoi.freya.game.Location;
import com.eoi.freya.util.SafeCast;

/**
 * Handles the take command. It transfers items from a Location Inventory to a Unit Inventory
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicTake extends BasicUnitPhase {

  public BasicTake()
  {
      command = BDef.TAKE;
  }
  public Element executeForUnit(BasicUnit unit, Element commandElement)
  {
    Element result = new Element(BDef.TAKE);
    String objectType = commandElement.getChildTextTrim(BDef.TYPE);
    int number = SafeCast.toInt(commandElement.getChildTextTrim(BDef.VALUE));
    if (number<1)
    {
        result.setAttribute("failed","true");
        return result;
    }

    Location loc = unit.getLocation();

    Inventory inventory = loc.getInventory();
    int current = inventory.getItemValue(objectType);

    Log.logger.info(""+unit.getName()+" tries to TAKE "+number+" "+objectType);

    // if the location has 1 or items
    if (current != 0)
    {
      // if trying to ytake more than location has, set number to the number here.
      if (number > current)
      {
        number = current;
      }
      // if number = 0, then take all that is here
      if (number == 0)
      {
        number = current;
      }

      current -= number;
      inventory.setItemValue(objectType,current);

      Inventory unitInventory = unit.getInventory();
      unitInventory.addToItemValue(objectType, number);

      Log.logger.info(""+number+" "+objectType+" taken.");
      result.setText(""+number+" "+objectType+" taken");
    }
    else
    {
      result.setAttribute("failed","true");
      result.setText("Nothing to take");
    }

    return result;

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/