package com.eoi.freya.basic;

import java.util.Iterator;

import org.jdom.Element;

import com.eoi.freya.player.Player;

/**
 * AllUnitPhase is a base class for code that work at a unit level. 
 * Child classes implement the executeForUnit(..) and is called for every unit 
 * of a player.
 * @version 1.0
 */

public abstract class AllUnitsPhase extends BasicPhase
{

  public void execute()
  {
    // get the Player object for this player
    Player player = getPlayer();
    Iterator i = player.getUnits();
    
    while(i.hasNext())
    {

      BasicUnit unit = (BasicUnit)i.next();

      Element result = executeForUnit(unit);
      result.detach();
      Element unitResult = getPlayerDocument().getResultForUnit(unit.getName());
      unitResult.addContent(result);

     
    }
  }

  public abstract Element executeForUnit(BasicUnit unit);
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
