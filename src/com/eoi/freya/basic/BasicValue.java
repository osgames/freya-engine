package com.eoi.freya.basic;

import org.jdom.Element;

/**
 * This class is an XML wrapper for integer values. They have a Max setting which increments cannot pass.
 * @author Sasha Bilton
 * @version 1.0
 */
 //e.g. <strength><value>10</value></strength>


public class BasicValue
{
  protected Element values;
  public BasicValue(Element values)
  {
    this.values = values;
  }

  public int getValue()
  {
    return Integer.parseInt(  values.getChildTextTrim(BDef.VALUE));
  }

  public void setValue(int value)
  {
    int max = getMax();
    if (value > max && max != 0)
    {
      value = getMax();
    }
    values.getChild(BDef.VALUE).setText(""+value);
  }

  public int getMax()
  {
    try
    {
      return Integer.parseInt(  values.getChildTextTrim(BDef.MAX));
    }
    catch(NullPointerException npe)
    {
      return 0;
    }
  }

  public void setMax(int value)
  {
    values.getChild(BDef.MAX).setText(""+value);
  }

  public void add(int addValue)
  {
    setValue( getValue() + addValue );
  }

  public void subtract(int subValue)
  {
    setValue( getValue() - subValue );
  }

  public String getName()
  {
    return values.getTextTrim();
  }

  public Element getElement()
  {
    return values;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/