package com.eoi.freya.basic;

import com.eoi.freya.player.Player;
import com.eoi.freya.turn.BasePhase;


/**
 * BasicPhase is a base class for Phases
 * @author Sasha Bilton
 * @version 1.0
 */

public abstract class BasicPhase extends BasePhase
{

  public BasicPhase()
  {
  }

  public Player getPlayer()
  {
        return BasicPlayerList.getInstance().getPlayer(getPlayerName());
  }

  public abstract void execute();

}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/