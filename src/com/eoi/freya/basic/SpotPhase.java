package com.eoi.freya.basic;

import java.util.Iterator;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.game.Location;

/**
 * SpotPhase outputs a list of other units at units location.
 * 
 * @version 1.0
 */

public class SpotPhase extends AllUnitsPhase
{

  public  Element executeForUnit(BasicUnit unit)
  {
	  boolean spotted = false;	//Have we seen another unit here?
	  Location unitLocation = unit.getLocation();
	  Iterator units = unitLocation.getUnits().iterator();
	  Element spot = new Element(BDef.SPOT);
	  while(units.hasNext()){
		  Element spotUnit = (Element)units.next();  
		  if (!spotUnit.getTextTrim().equals(unit.getName())){
			  Element unitElement = new Element(BDef.UNIT);
			  Element name = new Element(BDef.NAME);
			  name.setText(spotUnit.getText());
			  unitElement.addContent(name);
			  spot.addContent(unitElement);
			  spotted = true;
			  Log.logger.info(unit.getName()+" has spotted "
			  +spotUnit.getText());
		  }
	  }
	  return spot;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
