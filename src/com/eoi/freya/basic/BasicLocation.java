package com.eoi.freya.basic;

import java.util.Iterator;
import java.util.List;

import org.jdom.Element;

import com.eoi.freya.game.Inventory;
import com.eoi.freya.game.Location;
import com.eoi.freya.game.Unit;
/**
 * BasicLocation holds information on a specific place on a map.
 * It has an Inventory, Terrain, Position and a units on it.
 * @author Sasha Bilton
 * @version 1.0
 */


public class BasicLocation extends BasicPosition implements Location
{

  Inventory inventory = null;


  public BasicLocation()
  {
    super(new Element(BDef.LOCATION));
    Element inventory = new Element(BDef.INVENTORY);
    Element terrain = new Element(BDef.TERRAIN);
    values.addContent(inventory);
    values.addContent(terrain);

  }

  public BasicLocation(Element element)
  {
    super(element);
  }

  public String getTerrain()
  {
    return values.getChildText(BDef.TERRAIN);
  }

  public void setTerrain(String newTerrain)
  {
    Element terrain = values.getChild(BDef.TERRAIN);

    if (terrain == null)
    {
      terrain = new Element(BDef.TERRAIN);
      terrain.addContent(newTerrain);
      values.addContent(terrain);
    }
    else
    {
      terrain.setText(newTerrain);
    }
  }

  public void addUnit(Unit unit)
  {
    if (!this.hasUnit(unit.getName()))
    {
      String unitName = unit.getName();

      Element e = new Element(BDef.UNIT);
      e.setText(unitName);
      Element race = new Element(BDef.RACE);
      race.setText(((BasicUnit)unit).getRace());
      e.addContent(race);
      values.addContent(e);
    }
  }

  public void removeUnit(Unit unit)
  {
    String unitName = unit.getName();
    Element toRemove = null;
    Iterator i = getUnits().iterator();
    while (i.hasNext())
    {
      Element e = (Element)i.next();
      if(e.getTextTrim().equals(unitName))
      {
        i.remove();
	toRemove = e;
      }
    }
    if (toRemove!=null)
    {
	   values.removeContent(toRemove);
    }
  }
  public List getUnits()
  {
    return values.getChildren(BDef.UNIT);
  }

  /* not sure how useful this is */
  public boolean hasUnits()
  {
    if(values.getChildren(BDef.UNIT)!=null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public boolean hasUnit(String unitName)
  {
    Iterator i = getUnits().iterator();
    while (i.hasNext())
    {
      Element e = (Element)i.next();
      if(e.getTextTrim().equals(unitName))
      {
        return true;
      }
    }

     return false;
  }
  public Inventory getInventory()
  {
    if (inventory == null)
    {
      Element inv = values.getChild(BDef.INVENTORY);
      if (inv!= null)
      {
        inventory = new Inventory(inv);
      }
      else
      {
        inventory = new Inventory();
      }
    }
    return inventory;
  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
