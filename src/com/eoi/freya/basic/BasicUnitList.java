package com.eoi.freya.basic;

import java.util.HashMap;

import com.eoi.freya.game.Unit;
/**
 * BasicUnitList is used to hold references to all Units in the game.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicUnitList
{

  protected HashMap units;
  protected static BasicUnitList basicUnitList;

  protected BasicUnitList()
  {
    units = new HashMap();
  }

  public static BasicUnitList getInstance()
  {
    if (basicUnitList == null)
    {
      basicUnitList = new BasicUnitList();
    }

    return basicUnitList;
  }

  public void addUnit(Unit u)
  {
    units.put(u.getName(), u);
  }

  public Unit getUnit(String name)
  {
    return (Unit)units.get(name);
  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/