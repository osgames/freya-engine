package com.eoi.freya.basic;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.game.Inventory;
import com.eoi.freya.util.SafeCast;

/**
 * Handles the give command.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicGive extends BasicUnitPhase {

  public BasicGive()
  {
      command = BDef.GIVE;
  }
  public Element executeForUnit(BasicUnit unit, Element commandElement)
  {
    Element result = new Element(BDef.GIVE);
    String target =  commandElement.getChildTextTrim(BDef.TARGET);
    String objectType = commandElement.getChildTextTrim(BDef.TYPE);

    int number = SafeCast.toInt(commandElement.getChildTextTrim(BDef.VALUE));

    if (number<1)
    {
        result.setAttribute("failed","true");
        return result;
    }
    Inventory inventory = unit.getInventory();
    int current = inventory.getItemValue(objectType);

    Log.logger.info(""+unit.getName()+" tries to GIVE "+target+" "+number+" "+objectType);

    BasicUnitList bul = BasicUnitList.getInstance();
    BasicUnit targetUnit = (BasicUnit)bul.getUnit(target);

    // check that they are in the the same place
    if (unit.getPosition().equals(targetUnit.getPosition()))
    {

      // if the unit has 1 or items
      if (current != 0)
      {
        // if trying to give more than unit has, set number to the number available.
        if (number > current)
        {
          number = current;
        }
        // if number = 0, then give all there is
        if (number == 0)
        {
          number = current;
        }

        current -= number;
        inventory.setItemValue(objectType,current);


        Inventory targetInventory = targetUnit.getInventory();
        targetInventory.addToItemValue(objectType, number);

        Log.logger.info(""+number+" "+objectType+" give.");
        result.setText(""+number+" "+objectType+" given to "+target);
      }
      else
      {
        Log.logger.info("None to give.");
        result.setAttribute("failed","true");
      }
    }
    return result;

  }
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/