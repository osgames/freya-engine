package com.eoi.freya.basic;

/**
 * BDef holds all the constants for the BASIC game.
 * @author Sasha Bilton
 * @version 1.0
 */


public class BDef
{
  // Map
  public static final String X = "x";
  public static final String Y = "y";
  public static final String ROW = "row";
  public static final String MAP = "map";
  public static final String LOCATION = "location";
  public static final String TERRAIN = "terrain";
  public static final String NOT_DEFINED = "not defined";
  public static final String BLOCKED = "blocked";
  public static final String OPEN = "open";

  //Player
    public static final String NAME = "name";
    public static final String FILENAME = "filename";

  //Unit
    public static final String ATTACK = "attack";
    public static final String DEFENCE = "defence";
    public static final String HEALTH = "health";
    public static final String VALUE = "value";
    public static final String POSITION = "position";
    public static final String OWNER = "owner";
    public static final String INVENTORY = "inventory";
    public static final String MODIFIER = "modifier";
    public static final String DIRECTIONS = "directions";
    public static final String UNIT = "unit";
    public static final String UNITS_LIST = "units-list";
    public static final String RACE = "race";
    public static final String MAX = "max";

    //RemoveModifier
    public static final String BASIC = "basic";
    public static final String ATTRIBUTE = "attribute";
    public static final String TYPE = "type";
    public static final String COUNTER = "counter";

    //move
    public static final String MOVE = "move";
    public static final String DELIMITER = ",";
    public static final String  MOVEMENT_POINTS = "movement-points";

    //group
    public static final String GROUPS_LIST = "groups-list";
    public static final String GROUP = "group";

    //Retreat
    public static final String RETREAT = "retreat";

    //Combat
    public static final String FIGHT = "fight";
    public static final String TARGET = "target";
    public static final String DAMAGE = "damage";

    //Build
    public static final String BUILD = "build";

    public static final String COST = "cost";
    public static final String CAN_BUILD = "canBuild";

    //Repair
    public static final String REPAIR = "repair";

    //Take
    public static final String TAKE = "take";
    //Drop
    public static final String DROP = "drop";
    //Give
    public static final String GIVE = "give";

    //rest
    public static final String REST = "rest";

    //Player
    public static final String PLAYER = "player";


    //Reporting

    public static final String VISUAL_RANGE = "visual-range";
    public static final String STATS = "stats";

    //Mutate
    public static final String MUTATE = "mutate";

    //Rest
    public static final String HEAL_RATE = "heal-rate";

    //Spot
    public static final String SPOT = "spot";

    //Debug
    public static final String DEBUG = "debug"; 

}
/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
