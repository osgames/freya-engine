package com.eoi.freya.basic;

import java.util.Random;

import org.jdom.Element;

import com.eoi.freya.Log;

/**
 * BomberUnits blow up during combat. They damage as normal, but die themselves.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BomberUnit extends StandardUnit{

  public BomberUnit(Element info)
  {
    super(info);
  }

  public Element fight(BasicUnit target)
  {
    Random rng = new Random();

    Element result = new Element(BDef.ATTACK);

    int attack = getAttack().getValue();
    int defence = getDefence().getValue();
    int health = getHealth().getValue();

    int t_attack = target.getAttack().getValue();
    int t_defence = target.getDefence().getValue();
    int t_health = target.getHealth().getValue();

    int damage = (int)((attack - t_defence) * ((rng.nextDouble() / 2) +0.5));
    t_health = t_health - damage;

    //BOOM!
    getHealth().setValue(-1);
    target.getHealth().setValue(t_health);

    if (getHealth().getValue()<1)
    {
      this.setDead(true);
    }
    if (target.getHealth().getValue()<1)
    {
      target.setDead( true);
    }


    Element damageResult = new Element(BDef.DAMAGE);
    damageResult.setText(""+damage);

    Element unitName = new Element(BDef.UNIT);
    unitName.setText(this.getName());

    Element t_unitName = new Element(BDef.TARGET);
    t_unitName.setText(target.getName());

    result.addContent(unitName);
    result.addContent(damageResult);
    result.addContent(t_unitName);

    Log.logger.info(getName()+" did "+damage+" points to "+target.getName()+" and exploded");

    return result;

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/