package com.eoi.freya.basic;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.player.PlayerDocument;
import com.eoi.freya.player.PlayerDocumentList;

/**

 * The Combat Phase checks that the units are able to fight and in the same place and then hands over the combat to the units involved.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BasicCombat extends BasicUnitPhase
{

  public BasicCombat()
  {
    command = BDef.FIGHT;
  }

  public Element executeForUnit(BasicUnit unit, Element attackCommand)
  {
    Element result=null;
    String target = attackCommand.getChildTextTrim(BDef.TARGET);
    BasicMap map = BasicMap.getInstance();

    Log.logger.info(unit.getName()+" attacking "+target);

    BasicUnitList bul = BasicUnitList.getInstance();
    BasicUnit targetUnit = (BasicUnit)bul.getUnit(target);
    if (targetUnit != null)
    {
      if (unit.getPosition().equals(targetUnit.getPosition()))
      {
        result = unit.combat(targetUnit);
        PlayerDocument pd = PlayerDocumentList.getInstance().getPlayerDocument(targetUnit.getOwner());
        pd.getResultForUnit(targetUnit.getName()).addContent((Element)result.clone());
      }
      else
      {
        result = new Element(BDef.ATTACK);
        result.setAttribute("failed","true");
        result.setText("Target not found");
      }
    }
    // this has to be added the targets results as well.

    return result;

  }

}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/