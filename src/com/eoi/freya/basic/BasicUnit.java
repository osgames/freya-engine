package com.eoi.freya.basic;

import java.util.List;
import java.util.Random;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.game.BaseUnit;
import com.eoi.freya.game.Location;

/**
 * BasicUnit is a base class for all units in the game. It contains a number helper objects for values
 * such as Attack, Defence, etc. It also handles a simple form of fighting.
 * @author Sasha Bilton
 * @version 1.0
 */


public abstract class BasicUnit extends BaseUnit
{
  private BasicAttack attack = null;
  private BasicDefence defence = null;
  private BasicHealth health = null;
  private BasicValue movement = null;
  private BasicPosition position = null;
  private BasicValue retreat = null;
  private BasicValue cost = null;

  //Transient flags, internal to the game

  public transient boolean hasAttacked = false;
  public transient boolean hasMoved = false;



  public BasicUnit(Element unitInfo)
  {
    super(unitInfo);
  }

  public BasicAttack getAttack()
  {
    if (attack == null)
    {
      attack = new BasicAttack(info.getChild(BDef.ATTACK));
    }
    return attack;
  }

  public BasicDefence getDefence()
  {
    if (defence ==  null)
    {
     defence = new BasicDefence(info.getChild(BDef.DEFENCE));
    }
    return defence;
  }

  public BasicHealth getHealth()
  {
    if (health == null)
    {
      health = new BasicHealth(info.getChild(BDef.HEALTH));
    }
    return health;
  }

  public BasicPosition getPosition()
  {
    if (position == null)
    {
      position = new BasicPosition(info.getChild(BDef.POSITION));
    }
    return position;
  }

  public void setPosition(BasicPosition bp)
  {

    // if this unit doesn't have a position tag add it
    if (getPosition().getElement()==null)
    {
      position = bp;
      this.addElement(bp.getElement());
    }
    else
    // set the existing one
    {
      getPosition().setXY(bp.getX(), bp.getY());
    }

  }


  public BasicValue getMovementPoints()
  {
    if (movement == null)
    {
      movement = new BasicValue(info.getChild(BDef.MOVEMENT_POINTS));
    }
    return movement;
  }

  public BasicValue getRetreat()
  {
    if (retreat == null)
    {
      retreat = new BasicValue(info.getChild(BDef.RETREAT));
    }
    return retreat;
  }

  public BasicValue getCost()
  {
    if (cost == null)
    {
      cost = new BasicValue(info.getChild(BDef.COST));
    }
    return cost;
  }


  public String getRace()
  {
    return getSetting(BDef.RACE);
  }

  public List getModifiers()
  {
    return info.getChildren(BDef.MODIFIER);
  }

  public Location getLocation()
  {
    BasicMap map = BasicMap.getInstance();
    return map.getLocation(this.getPosition());
  }

  public Element combat(BasicUnit target)
  {
    if (hasAttacked)
    {
      Element r = new Element(BDef.ATTACK);
      r.setText("Unit has already attacked this round");
      return r;
    }
    else
    {
      hasAttacked = true;
      return fight(target);
    }
  }

   public Element fight(BasicUnit target)
  {
    Random rng = new Random();

    Element result = new Element(BDef.ATTACK);

    int attack = getAttack().getValue();
    int defence = getDefence().getValue();
    int health = getHealth().getValue();

    int t_attack = target.getAttack().getValue();
    int t_defence = target.getDefence().getValue();
    int t_health = target.getHealth().getValue();

    int damage = (int)((attack - t_defence) * ((rng.nextDouble() / 2) +0.5));
    t_health = t_health - damage;

    int t_damage = (int)((t_attack - defence) * ((rng.nextDouble() / 2) +0.5));
    health = health - t_damage;

    getHealth().setValue(health);
    target.getHealth().setValue(t_health);

    if (getHealth().getValue()<1)
    {
      setDead(true);
    }
    if (target.getHealth().getValue()<1)
    {
      target.setDead(true);
    }


    Element damageResult = new Element(BDef.DAMAGE);
    damageResult.setText(""+damage);

    Element t_damageResult = new Element(BDef.DAMAGE);
    t_damageResult.setText(""+t_damage);

    Element unitName = new Element(BDef.UNIT);
    unitName.setText(this.getName());

    Element t_unitName = new Element(BDef.TARGET);
    t_unitName.setText(target.getName());

    result.addContent(unitName);
    result.addContent(damageResult);
    result.addContent(t_unitName);
    result.addContent(t_damageResult);

    Log.logger.info(getName()+" did "+damage+" points to "+target.getName()+" and took "+t_damage);

    return result;



  }

  public abstract Element move(String directions, String modifier);


}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/