package com.eoi.freya;

import com.eoi.freya.turn.Turn;

/**
 * Game is the main class for freya-engine. It initialises everything, loads the config and runs a complete turn.
 * @author Sasha Bilton
 * @version 0.1
 */

public class Game {

  public void run(String configFile)
  {
    Initialise init = new Initialise(configFile);
    Turn turn = Turn.getInstance();
    turn.run();
  }


  public static void main(String[] args)
  {
    if (args.length != 1 || (args[0].indexOf("help")>0))
    {
      System.out.println("USAGE com.eoi.freya.Game <config file>");
    }
    else
    {
      Game game = new Game();
      game.run(args[0]);
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/