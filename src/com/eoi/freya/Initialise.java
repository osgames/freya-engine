package com.eoi.freya;
import com.eoi.freya.basic.BasicMap;
import com.eoi.freya.game.DataDictionary;
import com.eoi.freya.game.GameConfig;
import com.eoi.freya.game.Map;
import com.eoi.freya.game.ResultCodeConfig;
import com.eoi.freya.player.PlayerCommandList;
import com.eoi.freya.turn.Turn;
import com.eoi.freya.util.Config;


/**
 * Initialise loads up and creates all the data and singletons a game requires.
 * @author Sasha Bilton
 * @version 1.0
 */

public class Initialise {

  public Initialise(String configFile) throws InitialiseException
  {
    try
    {

      Config c = Config.loadFromFile(configFile);

      Config gc = (Config)GameConfig.loadFromFile(c.getSetting(Def.GAME_CONFIG_FILE));
	  Config rcg = (Config)ResultCodeConfig.loadFromFile (c.getSetting(Def.RESULT_CODE_CONFIG_FILE));


      Turn.loadTurnList(c.getSetting(Def.TURN_LIST));
      PlayerCommandList.loadPlayerList(c.getSetting(Def.PLAYER_COMMAND_LIST));
      DataDictionary.loadDataDictionaryList(c.getSetting(Def.DATA_DIC));

      Map map = BasicMap.getInstance();
      map.read(c.getSetting(Def.MAP));

    }
    catch (Exception e)
    {
      throw new InitialiseException("Failed to Initialise due to "+e);
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/