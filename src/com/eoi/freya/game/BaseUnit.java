package com.eoi.freya.game;

import org.jdom.Element;

import com.eoi.freya.Def;

/**
 * BaseUnits are the simplest instance of a  Unit. Extending OwnedObject gives them a name and owner. They can be alive or dead and have an Inventoey.
 * @author Sasha Bilton
 * @version 1.0
 */

public class BaseUnit extends Structure implements Unit
{

  // Inventory is where all objects the unit carries are stored
  private Inventory inventory = null;

  /** Units must have an Element to start with*/
  public BaseUnit(Element unitInfo)
  {
    super(unitInfo);
  }

  public boolean isDead() {
    return this.isSetting(Def.DEAD);
  }
  public void setDead(boolean isDead) {
    this.setBooleanSetting(Def.DEAD, isDead);
  }
	  
  
}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
