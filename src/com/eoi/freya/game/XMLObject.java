package com.eoi.freya.game;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.jdom.Element;

import com.eoi.freya.Def;
import com.eoi.freya.util.SafeCast;
/**
 * XMLObject is a simple object with a Name attribute and an element that contains information about that entity.
 * It's used as a base class for more specific objects.
 * NOTE : In general all set methods will create an element of that type if one doesn't exist,
 * while get methods throw NoSuchElement execeptions on unknown settings. getBoolean however returns false
 * if the setting is not found.
 *
 * @author     Sasha Bilton
 * @created    26 February 2002
 * @version    1.0
 */

public class XMLObject {
	/**
	 * info contains the info block that holds the XML representation of the object
	 */
	protected Element info;


	/**
	 *Constructor for the XMLObject object
	 *
	 * @param  e  Description of Parameter
	 */
	public XMLObject(Element e) {
		info = e;
	}


	/**
	 *Sets the info attribute of the XMLObject object
	 *
	 * @param  info  The new info value
	 */
	public void setInfo(Element info) {
		this.info = info;
	}


	/**
	 * @param  name  The String NAME of the object.
	 */
	public void setName(String name) {
		if (info.getChild(Def.NAME) != null) {
			info.getChild(Def.NAME).setText(name);
		} else {
			Element eName = new Element(Def.NAME);
			eName.setText(name);
		}
	}


	/**
	 * @return    The NAME of the object
	 */
	public String getName() {
		return info.getChildTextTrim(Def.NAME);
	}



	/**
	 * @param  setting                     The String name of the setting to change or create
	 * @param  value                       The String value to set the setting to.
	 * @exception  NoSuchElementException  Description of Exception
	 */
	public void setSetting(String setting, String value) throws NoSuchElementException {
		Element e = info.getChild(setting);
		if (e == null) {
			e = new Element(setting);
			info.addContent(e);
		}
		e.setText(value);
	}


	/**
	 *Sets the intSetting attribute of the XMLObject object
	 *
	 * @param  setting                     The new intSetting value
	 * @param  value                       The new intSetting value
	 * @exception  NoSuchElementException  Description of Exception
	 */
	public void setIntSetting(String setting, int value) throws NoSuchElementException {
		Element e = info.getChild(setting);
		if (e == null) {
			e = new Element(setting);
			info.addContent(e);
		}
		e.setText("" + value);
	}


	/**
	 *Sets the booleanSetting attribute of the XMLObject object
	 *
	 * @param  setting  The new booleanSetting value
	 * @param  value    The new booleanSetting value
	 */
	public void setBooleanSetting(String setting, boolean value) {
		Element e = info.getChild(setting);
		if (e == null) {
			e = new Element(setting);
			info.addContent(e);
		}
		e.setText(String.valueOf(value));
	}


	/**
	 * @param  setting                     Description of Parameter
	 * @return                             A new String containing the value of the setting
	 * @exception  NoSuchElementException  Description of Exception
	 */
	public String getSetting(String setting) throws NoSuchElementException {
		Element e = info.getChild(setting);
		if (e == null) {
			throw new NoSuchElementException(setting + " not found.");
		} else {
			return e.getTextTrim();
		}
	}


	/**
	 *Gets the intSetting attribute of the XMLObject object
	 *
	 * @param  setting                     Description of Parameter
	 * @return                             The intSetting value
	 * @exception  NoSuchElementException  Description of Exception
	 */
	public int getIntSetting(String setting) throws NoSuchElementException {
		Element e = info.getChild(setting);
		if (e == null) {
			throw new NoSuchElementException(setting + " not found.");
		} else {
			return SafeCast.toInt(e.getTextTrim());
		}
	}


	/**
	 *Gets the element attribute of the XMLObject object
	 *
	 * @param  name  Description of Parameter
	 * @return       The element value
	 */
	public Element getElement(String name) {
		Element e = info.getChild(name);
		if (e == null) {
			throw new NoSuchElementException(name + " not found.");
		} else {
			return e;
		}

	}


	/**
	 *Gets the iterator attribute of the XMLObject object
	 *
	 * @param  children  Description of Parameter
	 * @return           The iterator value
	 */
	public Iterator getIterator(String children) {
		return info.getChildren(children).iterator();
	}


	/**
	 *Gets the info attribute of the XMLObject object
	 *
	 * @return    The info value
	 */
	public Element getInfo() {
		return info;
	}


	/**
	 * Returns boolean true if the settings equals 'true', if otherwise false.
	 *  <B>NOTE that unfound element return false.</B>
	 *
	 * @param  setting  Description of Parameter
	 * @return          The setting value
	 */
	public boolean isSetting(String setting) {
		Element e = info.getChild(setting);
		if (e == null) {
			return false;
		} else {
			return Boolean.valueOf(e.getTextTrim()).booleanValue();
		}
	}


	/**
	 *Adds a feature to the Element attribute of the XMLObject object
	 *
	 * @param  e  The feature to be added to the Element attribute
	 */
	public void addElement(Element e) {
		info.addContent(e);
	}


	/**
	 *  Compares this XMLObject with another XMLObject and returns true if the names are the same.
	 *
	 * @param  o  Object to compare it with 
	 * @return    boolean value of result of comparason
	 */
	public boolean equals(Object o) {
		if (o instanceof XMLObject) {
			if (((Unit) o).getName().equals(this.getName())) {
				return true;
			}
		}
		return false;
	}
}

/*
 *  Freya Engine, The Turn Based Game Engine
 *  Copyright(c) 2002 Alexander Bilton
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

