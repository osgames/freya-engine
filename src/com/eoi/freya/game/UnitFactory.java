package com.eoi.freya.game;

import java.lang.reflect.Constructor;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.eoi.freya.Def;
import com.eoi.freya.Log;
import com.eoi.freya.util.Config;

/**
 * This class produces new units given a Race and Type and optionally the info block for the unit
 * @author Sasha Bilton
 * @version 1.0
 */

public class UnitFactory {

  static Document unitsClassMap;

  private UnitFactory() {
  }

  /**The Unit Class Map maps race/type combinations to Java classes. These mappings are stored as an XML file
   * which is loaded by this method.*/

  private static void loadMap()
  {
    try
    {
      Config gc = Config.getInstance();
      String filename = gc.getSetting(Def.UNIT_CLASS_MAP);
      Log.logger.info("Loading "+filename);
      SAXBuilder builder = new SAXBuilder();
      unitsClassMap = builder.build(filename);
      Log.logger.info("root = "+unitsClassMap);
    }
    catch(JDOMException ex)
    {
      Log.logger.severe(ex.toString());
    }
  }
  /** STATIC
   *  Create a new unit (BaseUnit) implementation based on the race and type of the unit specified.
   *  The info block of this unit is retrieved from the Data Dictionary.
   *  @param race The String name of the race of the new unit
   *  @param type The String name of the type of the new unit
   *  @return A new BaseUnit implementation of the new Unit.
   *  @see com.eoi.freya.game.BaseUnit
   */
   public static BaseUnit createNewUnit(String race, String type)
  {

    //Get the info block for this race/type
    Element info = (Element)(DataDictionary.getInstance().get("creature/"+race+"/"+type+"/unit")).clone();

    //detach the element from the dictionary
    info.detach();

    //check to see if the unitsClassMap has been loaded already
    if (unitsClassMap == null)
    {
      //load the Unit Class Map
      loadMap();
    }

    Log.logger.info("Trying to create a "+race+" "+type);
    Element root = unitsClassMap.getRootElement();
    // Get the full class name of the race/type
    String classname = root.getChild(race).getChildText(type);

    try
    {
      //Create a Class of the unit
      Class c = Class.forName(classname); // Create the class object

      //Create an array of types for the parameter list of the classes constructor
      Class[] paramTypes = {info.getClass()};
      //Create the param list using the info block gained from the Data Dictionary
      Object[] params = {info};
      //Create a constructor using the class and param types
      Constructor constructor = c.getConstructor(paramTypes);

      //Create a new instance of the unit class with the given parameters
      return (BaseUnit)constructor.newInstance(params);
    }
    catch (Exception e)
    {
      Log.logger.severe(e.toString());
      return null;
    }
  }

  /** STATIC
   *  Create a new unit (BaseUnit) implementation based on the race and type of the unit specified.
   *  The info block of this unit is defined by the caller
   *  @param race The String name of the race of the new unit
   *  @param type The String name of the type of the new unit
   *  @param unitInfo The Element info block to be used for the new unit.
   *  @return A new BaseUnit implementation of the new Unit.
   *  @see com.eoi.freya.game.BaseUnit
   */
  public static BaseUnit createNewUnit(String race, String type, Element unitInfo)
  {
    if (unitsClassMap == null)
    {
      loadMap();
    }

    Log.logger.info("Trying to create a "+race+" "+type);
    Element root = unitsClassMap.getRootElement();
    String classname = root.getChild(race).getChildText(type);

    try
    {
      Class c = Class.forName(classname); // Create the class object

      Class[] paramTypes = {unitInfo.getClass()};
      Object[] params = {unitInfo};
      Constructor constructor = c.getConstructor(paramTypes);


      return (BaseUnit)constructor.newInstance(params);
    }
    catch (Exception e)
    {
      Log.logger.severe(e.toString());
      return null;
    }
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


