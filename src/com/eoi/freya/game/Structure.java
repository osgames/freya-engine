package com.eoi.freya.game;

import org.jdom.Element;

import com.eoi.freya.Def;

/**
 *  Structure is a base class for structures.
 *
 * @author     Sasha Bilton
 * @created    30 May 2002
 */
public class Structure extends OwnedObject {

	private Inventory inventory = null;


	public Structure() {
		super(new Element(Def.STRUCTURE));
	}
	
	public Structure(Element root) {
		super(root);
	}
	

	/**
	 *  Gets the inventory attribute of the Structure object
	 *
	 * @return    The inventory value
	 */
	public Inventory getInventory() {
		// if this unit doesn't have an Inventory Item yet, create one
		if (inventory == null) {
			inventory = new Inventory(info.getChild(Def.INVENTORY));
		}
		return inventory;
	}
}

