package com.eoi.freya.util;

import java.util.Date;
import java.util.Random;

/**
 * DiceBox is a utility class that generates random numbers based on standard RPG notation.
 * this is generally xdy+z, where x is the number y sided dice to roll and z is a number to
 * add to the result.
 * E.G 2d6+2 rolls two 6 sided dice (values of 1-6) and adds 2 to the result.
 *
 * (Got to learn more regex to do this stuff using the new 1.4 JDK)
 * @author Sasha Bilton
 * @version 0.1
 */

public class DiceBox {

  public static int roll(String dice) throws NumberFormatException
  {
    Random random = new Random(new Date().getTime());

    // convert D's to d's
    dice = dice.toLowerCase();

    //get the number of dice to roll from the characters before the 'd'
    int dIndex = dice.indexOf("d");
    int numberOfDice = Integer.parseInt(dice.substring(0,dIndex));
    //get the type of dice
    int symbolIndex=dice.indexOf("+");

    int diceType;
    int modifier = 0;
    if (symbolIndex >0)
    {
      diceType = Integer.parseInt(dice.substring(dIndex+1,symbolIndex));
      modifier = Integer.parseInt(dice.substring(symbolIndex+1, dice.length()));
    }
    else
    {
      diceType = Integer.parseInt(dice.substring(dIndex+1,dice.length()));
    }

    int result=0;

    for (int i=1; i<=numberOfDice; i++)
    {
      result += random.nextInt(diceType-1)+1;
    }

    result += modifier;

    return result;

  }

  public static void main(String args[])
  {
    System.out.println(DiceBox.roll("3d6"));
    System.out.println(DiceBox.roll("1d4+1"));
    System.out.println(DiceBox.roll("1d100"));

  }

}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/