package com.eoi.freya.player;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.eoi.freya.Def;


/**
 * Each player submits an XML based command list, this is encapsulated by the PlayerCommands class.
 * @author Sasha Bilton
 * @version 1.0
 */

public class PlayerCommands {

  private Element rootElement;

  public PlayerCommands(String filename)
  {
    try
    {

      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(filename);
      rootElement = d.getRootElement();
    }
    catch(JDOMException ex)
    {
      System.out.println(ex);
      //Log.error("Config - "+e);
    }
  }

  public String getPlayerName()
  {
    return rootElement.getChildTextTrim(Def.NAME);
  }

  public int getCommandTurn()
  {
    return Integer.parseInt(rootElement.getChildTextTrim(Def.TURN));
  }

  public Element getCommandRoot()
  {
    return rootElement.getChild(Def.COMMANDS);
  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/