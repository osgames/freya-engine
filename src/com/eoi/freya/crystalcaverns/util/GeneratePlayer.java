package com.eoi.freya.crystalcaverns.util;

import java.util.Iterator;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.eoi.freya.Log;
import com.eoi.freya.basic.BDef;
import com.eoi.freya.basic.BasicMap;
import com.eoi.freya.basic.BasicPlayer;
import com.eoi.freya.basic.BasicPosition;
import com.eoi.freya.basic.BasicUnit;
import com.eoi.freya.crystalcaverns.CCDef;
import com.eoi.freya.game.DataDictionary;
import com.eoi.freya.game.UnitFactory;
import com.eoi.freya.util.Config;



/**
 * Stand alone app for generating a Player xml docs
 * @author Sasha Bilton
 * @version 1.0
 */

public class GeneratePlayer {

  private BasicPlayer player;

  public void makeUnits(String playerName, String race, int x, int y, String scoutName, String motherName)
  {
    player = new BasicPlayer();
    player.setName(playerName);
    player.setRace(race);

    BasicPosition bp = new BasicPosition(x,y);

    BasicUnit unit1 = (BasicUnit)UnitFactory.createNewUnit(race,"scout");
    unit1.setName(scoutName);
    unit1.setPosition(bp);
    unit1.setOwner(playerName);


    BasicUnit unit2 = (BasicUnit)UnitFactory.createNewUnit(race,"mother");
    unit2.setName(motherName);
    unit2.setPosition(bp);
    unit2.getInventory().addItemValue(CCDef.CRYSTALS,5);
      unit2.setOwner(playerName);

    player.addUnit(unit2);
    player.addUnit(unit1);
    player.write();

  }

  public void go(String defFile)
  {
    try
    {
      Config.loadFromFile("xml/config.xml");
      DataDictionary.loadDataDictionaryList("xml/datadic.xml");

      SAXBuilder builder = new SAXBuilder();
      Document d = builder.build(defFile);
      Element rootElement = d.getRootElement();

      Iterator i = rootElement.getChildren(BDef.PLAYER).iterator();

      BasicMap map = BasicMap.getInstance();
      map.read("xml/map.xml");

      int players = rootElement.getChildren(BDef.PLAYER).size();


      while (i.hasNext())
      {
        Element player = (Element)i.next();
        String playerName = player.getChildTextTrim(BDef.NAME);
        String race = player.getChildTextTrim(BDef.RACE);
        String mother = player.getChildTextTrim("mother");
        String scout = player.getChildTextTrim("scout");
        int x = Integer.parseInt(player.getChildTextTrim(BDef.X));

        int y = Integer.parseInt(player.getChildTextTrim(BDef.Y));
        for (int lx =-1; lx<2 ; lx++)
        {
          for (int ly =-1; ly<2 ; ly++)
          {
            map.getLocation(x+lx,y+ly).setTerrain(BDef.OPEN);
          }
        }
        makeUnits(playerName,race,x,y,scout,mother);
      }
    map.write("xml/map.xml");
    }
    catch (Exception e)
    {
      Log.logger.severe(""+e);
    }
  }

  public static void main(String args[])
  {
    GeneratePlayer gp = new GeneratePlayer();
    gp.go("xml/playerdefs.xml");
  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
