package com.eoi.freya.crystalcaverns.phase;

import org.jdom.Element;

import com.eoi.freya.basic.BDef;
import com.eoi.freya.basic.BasicUnit;
import com.eoi.freya.basic.BasicUnitList;
import com.eoi.freya.basic.BasicUnitPhase;
import com.eoi.freya.crystalcaverns.CCDef;
import com.eoi.freya.game.Inventory;

/**
 * The Repair command allows certain units to repair named units using crystals.
 * @author Sasha Bilton
 * @version 1.0
 */

public class CCRepair extends BasicUnitPhase
{

  public CCRepair()
  {
    command = BDef.REPAIR;
  }

  public Element executeForUnit(BasicUnit unit, Element repairCommand)
  {
    Element result = new Element(BDef.REPAIR);

    // Robots only can repair, units that have attacked or moved can't repair.
    if (unit.getRace().equals(CCDef.ROBOT) || !unit.hasAttacked || !unit.hasMoved)
    {
      Inventory inventory = unit.getInventory();

      int crystals = inventory.getItemValue(CCDef.CRYSTALS);
      int use = Integer.parseInt(repairCommand.getChildTextTrim(CCDef.CRYSTALS));

      if( use > crystals || use == 0)
      {
        use = crystals;
      }

      String target = repairCommand.getChildTextTrim(BDef.TARGET);
      BasicUnitList bul = BasicUnitList.getInstance();
      BasicUnit targetUnit = (BasicUnit)bul.getUnit(target);

      if (unit.getPosition().equals(targetUnit.getPosition()) && use > 0)
      {
        int heal = use * 100;
        targetUnit.getHealth().add(heal);
        result.setText("repaired "+heal+" points of damage to "+targetUnit.getName());
      }
      else
      {
        result.setAttribute("failed","true");
        result.setText("failed");
      }
    }
    else
    {
      result.setAttribute("failed","true");
      result.setText("Unit is not a builder");
    }
    return result;
  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/