package com.eoi.freya.crystalcaverns.phase;

import java.util.Random;

import org.jdom.Element;

import com.eoi.freya.Log;
import com.eoi.freya.basic.BasicLocation;
import com.eoi.freya.basic.BasicMap;
import com.eoi.freya.basic.BasicPhase;
import com.eoi.freya.crystalcaverns.CCDef;
import com.eoi.freya.game.Inventory;
/**
 * Grow Crystals is a Phase that changes the crystal layout of a map.

 * @author Sasha Bilton
 * @version 1.0

 Conways Life, which GrowCrystals is vaguely based on..
  Death If an occupied cell has 0, 1, 4, 5, 6, 7, or 8 occupied neighbors, the organism dies
(0, 1 neighbors: of loneliness; 4 thru 8: of overcrowding).

  Survival If an occupied cell has two or three neighbors, the organism survives to the next generation.

  Birth If an unoccupied cell has three occupied neighbors, it becomes occupied.
*/
public class GrowCrystals extends BasicPhase {

  public GrowCrystals()
  {
    repeat = false;
  }

  public void execute()
  {
    int assumedAverage = 2;
    int born =0;
    int die = 0;


    BasicMap map = BasicMap.getInstance();

    int max_x = map.getMaxX();
    int max_y = map.getMaxY();

    BasicLocation[][] newMap = new BasicLocation[max_x][max_y];
    Inventory i = null;
    Inventory inventory = null;

    for(int y=0;y<max_y;y++)
    {
      for(int x=0;x<max_x;x++)
      {

        //copy everything to new map location
        newMap[x][y] = new BasicLocation((Element)((BasicLocation)map.getLocation(x,y)).getElement().clone());

        // get the inventory of the location
        inventory = newMap[x][y].getInventory();


        int surrounding = 0;
        //this breaks Conways algorithm
         i = map.getLocation(x,y).getInventory();
         surrounding += i.getItemValue(CCDef.CRYSTALS);

        if (x>0)
        {
          if (y>0)
          {
            i = map.getLocation(x-1,y-1).getInventory();
            surrounding += i.getItemValue(CCDef.CRYSTALS);
          }

          i = map.getLocation(x-1,y).getInventory();
          surrounding += i.getItemValue(CCDef.CRYSTALS);

          if (y<max_y-1)
          {
            i = map.getLocation(x-1,y+1).getInventory();
            surrounding += i.getItemValue(CCDef.CRYSTALS);
          }
        }

        if(y>0)
        {
          i = map.getLocation(x,y-1).getInventory();
          surrounding += i.getItemValue(CCDef.CRYSTALS);
        }
        if (y<max_y-1)
        {
          i = map.getLocation(x,y+1).getInventory();
          surrounding += i.getItemValue(CCDef.CRYSTALS);
        }
        if(x<max_x-1)
        {
          if (y>0)
            {
              i = map.getLocation(x+1,y-1).getInventory();
              surrounding += i.getItemValue(CCDef.CRYSTALS);
            }

            i = map.getLocation(x+1,y).getInventory();
            surrounding += i.getItemValue(CCDef.CRYSTALS);

            if (y<max_y-1)
            {
              i = map.getLocation(x+1,y+1).getInventory();
              surrounding += i.getItemValue(CCDef.CRYSTALS);
            }
          }

          //Log.logger.info("Square "+x+" "+y+" has "+surrounding+" surrounding crystals");

          // divide the surrounding total by a guessed average of the value of a 'live' location
          surrounding = surrounding / assumedAverage;

          if (surrounding < 2)
          {
            if (inventory.getItemValue(CCDef.CRYSTALS)> 0)
            {
              inventory.decreaseFromItemValue(CCDef.CRYSTALS, 1);
              die +=1;
            }

          }
          else
          if (surrounding > 3)
          {
            if (inventory.getItemValue(CCDef.CRYSTALS)> 0)
            {
              inventory.decreaseFromItemValue(CCDef.CRYSTALS,1);
              die += 1;
            }
          }
          else
          if (surrounding == 3)
          {
            inventory.addToItemValue(CCDef.CRYSTALS, 1);
            born +=1;
          }



        }
      }
      Log.logger.info("Born = "+born+", die = "+die);

      // now add some random crystal seeds...
      if (die > born)
      {
          Random rng = new Random();

        for (int newBorn =born; newBorn <die; newBorn++)
        {
           int newX = rng.nextInt(max_x);
          int newY = rng.nextInt(max_y);
          newMap[newX][newY].getInventory().addToItemValue(CCDef.CRYSTALS,rng.nextInt(3));
          Log.logger.info("Added a crystal to "+newX+" "+newY);
        }
      }

      map.setMap(newMap);

  }
}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/