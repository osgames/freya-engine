package com.eoi.freya.turn;

import java.util.List;

import org.jdom.Element;

import com.eoi.freya.Def;
import com.eoi.freya.game.XMLObject;
import com.eoi.freya.player.Player;
import com.eoi.freya.player.PlayerCommands;
import com.eoi.freya.player.PlayerDocument;
import com.eoi.freya.player.PlayerDocumentList;
/**
 * BasePhase can be used as basis for more complex phase classes. The main method for phases is the execute method. This class gains access to the current players commands via the setPlayerCommand method, which is called by the Turn class for each Phase
 * @author Sasha Bilton
 * @version 1.0
 */



public abstract class BasePhase extends XMLObject implements Phase{

  protected PlayerCommands playerCommands;
  protected Player player;

  protected boolean repeat=true;

  public BasePhase()
  {
    super(new Element(Def.ATTRIBUTES));
  }

  public void setPlayerCommands(PlayerCommands playerCommands)
  {
    this.playerCommands = playerCommands;
  }

  public PlayerCommands getPlayerCommands()
  {
    return playerCommands;
  }

  public PlayerDocument getPlayerDocument(String name)
  {
    return PlayerDocumentList.getInstance().getPlayerDocument(name);
  }

  public PlayerDocument getPlayerDocument()
  {
    return PlayerDocumentList.getInstance().getPlayerDocument(playerCommands.getPlayerName());
  }


  public List getCommands(String command)
  {
    return playerCommands.getCommandRoot().getChildren(command);
  }

  public String getPlayerName()
  {
    return getPlayerCommands().getPlayerName();
  }

  public Player getPlayer()
  {
    return player;
  }
  public void setPlayer(Player player)
  {
    this.player = player;
  }

  public boolean repeatForAllPlayers()
  {
    return repeat;
  }

  public abstract void execute();

}

/*
Freya Engine, The Turn Based Game End
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/