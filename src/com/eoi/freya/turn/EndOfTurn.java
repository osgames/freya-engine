package com.eoi.freya.turn;

import com.eoi.freya.Def;
import com.eoi.freya.Log;
import com.eoi.freya.basic.BasicMap;
import com.eoi.freya.basic.BasicPlayerList;
import com.eoi.freya.player.PlayerDocumentList;
import com.eoi.freya.util.Config;

/**
 * @author
 * @version 1.0
 */

public class EndOfTurn extends BasePhase {


  public EndOfTurn() {
  repeat = false;
  }

  public void execute()
  {

    Log.logger.info("Writing player files.");
    BasicPlayerList pl = BasicPlayerList.getInstance();
    pl.writePlayers();

    PlayerDocumentList pdl = PlayerDocumentList.getInstance();
    pdl.writePlayerDocs();

    BasicMap.getInstance().write(Config.getInstance().getSetting(Def.MAP));

  }

}

/*
Freya Engine, The Turn Based Game Engine
Copyright(c) 2002 Alexander Bilton
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/